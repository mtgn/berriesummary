﻿using BerrieSummary;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using BerrieSummary.Harvests;
using BerrieSummary.Payments;
using BerrieSummary.Workers;
using System.Globalization;
using System.Threading.Tasks;

namespace BerrieSummary
{
    public partial class MainForm : Form
    {
        HarvestsForm m_harvestsForm = null;
        PaymentsForm m_paymentsForm = null;
        WorkersForm m_workersForm = null;

        public MainForm()
        {
            InitializeComponent();

            SetPlantationsControl();
            SetFileSync();
        }

        private void SetFileSync()
        {
            FileSyncHelper.OnFilesImported += FileSyncHelper_OnFilesImported;
            FileSyncHelper.OnFilesExported += FileSyncHelper_OnFilesExported;
            FileSyncHelper.StartSync();
        }

        private void SetPlantationsControl()
        {
            plantationsDDL.ComboBox.DataSource = new PlantationsCache().ToList();
            plantationsDDL.ComboBox.DisplayMember = "Name";
            plantationsDDL.ComboBox.ValueMember = "PlantationId";
            plantationsDDL.ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void FileSyncHelper_OnFilesExported(object sender, EventArgs e)
        {
            ShowExportSummaryMessageBox();
        }

        private static DialogResult ShowExportSummaryMessageBox()
        {
            return MessageBox.Show("Eskport zakończony powodzeniem.\nListy pracowników na skanerze zostały zaktualizowane.",
                "Podsumowanie eksportu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void FileSyncHelper_OnFilesImported(object sender, EventArgs e)
        {
            ChangeSyncStatus(true);
        }

        private void ChangeSyncStatus(bool? value = null)
        {
            if(!value.HasValue)
            {
                value = !SyncStatusLabel.Visible;
            }

            if (SyncStatusLabel.InvokeRequired)
            {
                SyncStatusLabel.Invoke(new MethodInvoker(() => { SyncStatusLabel.Visible = value.Value; }));
            }
            else
            {
                SyncStatusLabel.Visible = value.Value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            using (var form = new SyncForm())
            {
                Cursor = Cursors.Default;
                try
                {
                    if (form.ShowDialog() != DialogResult.None)
                    {
                        ChangeSyncStatus(false);
                    }
                }
                catch (ObjectDisposedException)
                {
                }
            }
        }

        private void HarvestsFormButton_Click(object sender, EventArgs e)
        {
            if (m_harvestsForm == null)
            {
                m_harvestsForm = new HarvestsForm((int)plantationsDDL.ComboBox.SelectedValue);
                m_harvestsForm.FormClosed += M_harvestsForm_FormClosed;
                m_harvestsForm.Show();
            }
            else
            {
                if(m_harvestsForm.WindowState == FormWindowState.Minimized)
                {
                    m_harvestsForm.WindowState = FormWindowState.Normal;
                }

                m_harvestsForm.BringToFront();
                m_harvestsForm.Focus();
            }
        }

        private void M_harvestsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_harvestsForm = null;
        }

        private void PaymentsFormButton_Click(object sender, EventArgs e)
        {
            if (m_paymentsForm == null)
            {
                m_paymentsForm = new PaymentsForm((int)plantationsDDL.ComboBox.SelectedValue);
                m_paymentsForm.FormClosed += M_paymentsForm_FormClosed;
                m_paymentsForm.Show();
            }
            else
            {
                if (m_paymentsForm.WindowState == FormWindowState.Minimized)
                {
                    m_paymentsForm.WindowState = FormWindowState.Normal;
                }

                m_paymentsForm.BringToFront();
                m_paymentsForm.Focus();
            }
        }

        private void M_paymentsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_paymentsForm = null;
        }

        private void WorkersFormButton_Click(object sender, EventArgs e)
        {
            if (m_workersForm == null)
            {
                m_workersForm = new WorkersForm((int)plantationsDDL.ComboBox.SelectedValue);
                m_workersForm.FormClosed += M_workersForm_FormClosed;
                m_workersForm.Show();
            }
            else
            {
                if (m_workersForm.WindowState == FormWindowState.Minimized)
                {
                    m_workersForm.WindowState = FormWindowState.Normal;
                }

                m_workersForm.BringToFront();
                m_workersForm.Focus();
            }
        }

        private void M_workersForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            m_workersForm = null;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ta opcja umożliwia zresetowanie bazy danych. \n\nCzy chcesz kontynuować?",
                "Uwaga", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                var reset = new ResetForm();
                reset.ShowDialog();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            FileSyncHelper.StopSync();
            Enabled = false;
            if (DatabaseHelper.HasChanged)
            {
                using (var backupForm = new BackupForm())
                {
                    backupForm.ShowDialog();
                }
            }
        }

        private void ustawieniaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var settingsForm = new SettingsForm())
            {
                settingsForm.ShowDialog();
            }
        }
    }
}
