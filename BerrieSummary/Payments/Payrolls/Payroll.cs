﻿using System;

namespace BerrieSummary.Payments
{
    public class Payroll
    {
        public int PaymentId { get; set; }
        public int WorkerId { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public decimal BasketsQuantity { get; set; }
        public decimal? Premium { get; set; }
        public decimal WorkValue { get; set; }

        public decimal TotalValue
        {
            get
            {
                return Math.Ceiling(WorkValue + (Premium ?? 0));
            }
        }
        public string BasketsQuantityString
        {
            get
            {
                return BasketsQuantity.ToString("0.0");
            }
        }

        public string BasketRateString
        {
            get
            {
                return (WorkValue / BasketsQuantity).ToString("0.00 zł");
            }
        }

        public string TotalValueString
        {
            get
            {
                return TotalValue.ToString("0 zł");
            }
        }

        public string WorkValueString
        {
            get
            {
                return Math.Ceiling(WorkValue).ToString("0 zł");
            }
        }

        public string PremiumString
        {
            get
            {
                return Premium.HasValue
                    ? Premium.Value != 0 
                        ? Premium.Value.ToString("0 zł") 
                        : string.Empty
                    : string.Empty;
            }
        }

        public WorkerPayment GetWorkerPayment()
        {
            return new WorkerPayment()
            {
                PaymentId = PaymentId,
                WorkerId = WorkerId,
                Premium = Premium ?? 0
            };
        }
    }
}
