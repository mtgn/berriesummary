﻿using BerrieSummary.Sync;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary.Workers
{
    public partial class WorkerHistoryForm : Form
    {
        private int m_plantationId = -1;
        private int m_codeId = -1;
        private WorkerHistoryHelper m_workerHistoryHelper;

        public DateTime? SelectedDate
        {
            get
            {
                if (WorkerHistoryGrid.SelectedRows.Count == 0)
                {
                    return null;
                }

                var row = WorkerHistoryGrid.SelectedRows[0];
                return (row.DataBoundItem as WorkerDailySummary).Date;
            }
        }

        private WorkerHistoryForm()
        {
            InitializeComponent();
            WorkerHistoryGrid.AutoGenerateColumns = false;
        }

        public WorkerHistoryForm(int plantationId, int codeId)
            : this()
        {
            m_plantationId = plantationId;
            m_codeId = codeId;
            m_workerHistoryHelper = new WorkerHistoryHelper(plantationId, codeId);
            WorkerHistoryGrid.DataSource = m_workerHistoryHelper.History;

            NameLabel.Text = string.Format("{0}\n{1}", m_workerHistoryHelper.FirstName, m_workerHistoryHelper.LastName);
            CodeLabel.Text = m_workerHistoryHelper.Code;
        }

        private void ChangeBasketsButton_Click(object sender, EventArgs e)
        {
            using (var form = new ChangeBasketsForm(m_plantationId, m_codeId, SelectedDate))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.No)
                {
                    MessageBox.Show("Nie udało się wykonać korekty.", "Korekta ilości koszyków", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (result == DialogResult.OK)
                {
                    RefreshGrid();
                }
            }
        }

        private void ChangeWorkTimeButton_Click(object sender, EventArgs e)
        {
            using (var form = new ChangeWorkTimeForm(m_plantationId, m_codeId, SelectedDate))
            {
                var result = form.ShowDialog();
                if (result == DialogResult.No)
                {
                    MessageBox.Show("Nie udało się wykonać korekty.", "Korekta czasu pracy", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (result == DialogResult.OK)
                {
                    RefreshGrid();
                }
            }
        }

        private void RefreshGrid()
        {
            m_workerHistoryHelper.RefreshCache();
            WorkerHistoryGrid.DataSource = null;
            WorkerHistoryGrid.DataSource = m_workerHistoryHelper.History;
        }
    }
}
