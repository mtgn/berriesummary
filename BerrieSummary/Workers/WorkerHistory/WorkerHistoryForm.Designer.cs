﻿namespace BerrieSummary.Workers
{
    partial class WorkerHistoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkerHistoryForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.WorkerHistoryGrid = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CodeLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.ChangeBasketsButton = new System.Windows.Forms.Button();
            this.ChangeWorkTimeButton = new System.Windows.Forms.Button();
            this.whDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whBasketsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whWorkTimeCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whEfficiency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whBasketRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whTotalValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.whHourRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkerHistoryGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.Controls.Add(this.WorkerHistoryGrid, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 520);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // WorkerHistoryGrid
            // 
            this.WorkerHistoryGrid.AllowUserToAddRows = false;
            this.WorkerHistoryGrid.AllowUserToDeleteRows = false;
            this.WorkerHistoryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WorkerHistoryGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.whDate,
            this.whBasketsCount,
            this.whWorkTimeCount,
            this.whEfficiency,
            this.whBasketRate,
            this.whTotalValue,
            this.whHourRate});
            this.WorkerHistoryGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkerHistoryGrid.Location = new System.Drawing.Point(3, 3);
            this.WorkerHistoryGrid.MultiSelect = false;
            this.WorkerHistoryGrid.Name = "WorkerHistoryGrid";
            this.WorkerHistoryGrid.ReadOnly = true;
            this.WorkerHistoryGrid.RowHeadersVisible = false;
            this.WorkerHistoryGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WorkerHistoryGrid.Size = new System.Drawing.Size(581, 514);
            this.WorkerHistoryGrid.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.ChangeBasketsButton);
            this.panel1.Controls.Add(this.ChangeWorkTimeButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(590, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 514);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Orange;
            this.panel2.Controls.Add(this.CodeLabel);
            this.panel2.Controls.Add(this.NameLabel);
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 112);
            this.panel2.TabIndex = 14;
            // 
            // CodeLabel
            // 
            this.CodeLabel.AutoSize = true;
            this.CodeLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CodeLabel.Location = new System.Drawing.Point(43, 18);
            this.CodeLabel.Margin = new System.Windows.Forms.Padding(10);
            this.CodeLabel.Name = "CodeLabel";
            this.CodeLabel.Size = new System.Drawing.Size(61, 21);
            this.CodeLabel.TabIndex = 15;
            this.CodeLabel.Text = "0-0000";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NameLabel.Location = new System.Drawing.Point(42, 49);
            this.NameLabel.Margin = new System.Windows.Forms.Padding(10);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(92, 50);
            this.NameLabel.TabIndex = 14;
            this.NameLabel.Text = "Imię\r\nNazwisko\r\n";
            // 
            // ChangeBasketsButton
            // 
            this.ChangeBasketsButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ChangeBasketsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeBasketsButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ChangeBasketsButton.ForeColor = System.Drawing.Color.Black;
            this.ChangeBasketsButton.Image = global::BerrieSummary.Properties.Resources.basket;
            this.ChangeBasketsButton.Location = new System.Drawing.Point(35, 334);
            this.ChangeBasketsButton.Name = "ChangeBasketsButton";
            this.ChangeBasketsButton.Size = new System.Drawing.Size(164, 142);
            this.ChangeBasketsButton.TabIndex = 11;
            this.ChangeBasketsButton.Text = "Koryguj ilość koszyków";
            this.ChangeBasketsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ChangeBasketsButton.UseVisualStyleBackColor = false;
            this.ChangeBasketsButton.Click += new System.EventHandler(this.ChangeBasketsButton_Click);
            // 
            // ChangeWorkTimeButton
            // 
            this.ChangeWorkTimeButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ChangeWorkTimeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeWorkTimeButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ChangeWorkTimeButton.ForeColor = System.Drawing.Color.Black;
            this.ChangeWorkTimeButton.Image = global::BerrieSummary.Properties.Resources.time;
            this.ChangeWorkTimeButton.Location = new System.Drawing.Point(35, 164);
            this.ChangeWorkTimeButton.Name = "ChangeWorkTimeButton";
            this.ChangeWorkTimeButton.Size = new System.Drawing.Size(164, 142);
            this.ChangeWorkTimeButton.TabIndex = 10;
            this.ChangeWorkTimeButton.Text = "Koryguj czas pracy";
            this.ChangeWorkTimeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ChangeWorkTimeButton.UseVisualStyleBackColor = false;
            this.ChangeWorkTimeButton.Click += new System.EventHandler(this.ChangeWorkTimeButton_Click);
            // 
            // whDate
            // 
            this.whDate.DataPropertyName = "DateString";
            this.whDate.HeaderText = "Data";
            this.whDate.Name = "whDate";
            this.whDate.ReadOnly = true;
            this.whDate.Width = 80;
            // 
            // whBasketsCount
            // 
            this.whBasketsCount.DataPropertyName = "BasketsQuantityString";
            this.whBasketsCount.HeaderText = "Zebranych koszyków";
            this.whBasketsCount.Name = "whBasketsCount";
            this.whBasketsCount.ReadOnly = true;
            this.whBasketsCount.Width = 83;
            // 
            // whWorkTimeCount
            // 
            this.whWorkTimeCount.DataPropertyName = "WorkTimeString";
            this.whWorkTimeCount.HeaderText = "Czas pracy";
            this.whWorkTimeCount.Name = "whWorkTimeCount";
            this.whWorkTimeCount.ReadOnly = true;
            this.whWorkTimeCount.Width = 70;
            // 
            // whEfficiency
            // 
            this.whEfficiency.DataPropertyName = "EfficiencyString";
            this.whEfficiency.HeaderText = "% normy";
            this.whEfficiency.Name = "whEfficiency";
            this.whEfficiency.ReadOnly = true;
            this.whEfficiency.Width = 65;
            // 
            // whBasketRate
            // 
            this.whBasketRate.DataPropertyName = "BasketRateString";
            this.whBasketRate.HeaderText = "Stawka za koszyk (ustalona)";
            this.whBasketRate.Name = "whBasketRate";
            this.whBasketRate.ReadOnly = true;
            this.whBasketRate.Width = 80;
            // 
            // whTotalValue
            // 
            this.whTotalValue.DataPropertyName = "TotalValueString";
            this.whTotalValue.HeaderText = "Należność za dzień";
            this.whTotalValue.Name = "whTotalValue";
            this.whTotalValue.ReadOnly = true;
            this.whTotalValue.Width = 85;
            // 
            // whHourRate
            // 
            this.whHourRate.DataPropertyName = "HourRateString";
            this.whHourRate.HeaderText = "Stawka za godzinę (obliczona)";
            this.whHourRate.Name = "whHourRate";
            this.whHourRate.ReadOnly = true;
            this.whHourRate.Width = 88;
            // 
            // WorkerHistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 520);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "WorkerHistoryForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Historia pracy";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WorkerHistoryGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView WorkerHistoryGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ChangeBasketsButton;
        private System.Windows.Forms.Button ChangeWorkTimeButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label CodeLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn whDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn whBasketsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn whWorkTimeCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn whEfficiency;
        private System.Windows.Forms.DataGridViewTextBoxColumn whBasketRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn whTotalValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn whHourRate;
    }
}