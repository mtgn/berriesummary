﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace BerrieSummary
{
    static class Program
    {
        internal const string DATABASE_NAME = "Blueberries";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            EnsureDatabaseExists();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void EnsureDatabaseExists()
        {
            try
            {
                var dbExists = CheckDatabaseExists();
                if (!dbExists)
                {
                    Console.WriteLine("[BS] Creating the database...");
                    if (CreateDatabase())
                    {
                        Console.WriteLine("[BS] The database has been created.");
                    }
                    else
                    {
                        Console.WriteLine("[BS] Could not create the database.");
                    }
                }
                else
                {
                    Console.WriteLine("[BS] The database server is connected.");
                }
            }
            catch(Exception)
            {
                Console.WriteLine("[BS] An exception occured while initializing the database.");
            }
        }

        private static bool CreateDatabase()
        {
            using (var connection = new SqlConnection("server=(local)\\SQLEXPRESS;Trusted_Connection=yes"))
            {
                connection.Open();
                using (var command = new SqlCommand($"create database {DATABASE_NAME}; ALTER DATABASE {DATABASE_NAME} SET RECOVERY FULL;", connection))
                {
                    command.ExecuteScalar();
                }

                string cmdText = $"use {DATABASE_NAME}; Create table [PLANTATIONS]([PLANTATION_ID] Integer Identity NOT NULL, UNIQUE ([PLANTATION_ID]), [NAME] Varchar(50) NOT NULL, [BASE_RATE] Decimal(8, 2) NOT NULL, Primary Key ([PLANTATION_ID]));"
                + "Create table [BASKETS]([BASKET_ID] Integer Identity NOT NULL, [PLANTATION_ID] Integer NOT NULL,	[CODE_ID] Integer NOT NULL,	[QUANTITY] Decimal(8,2) NOT NULL, [COLLECTION_DATE] Datetime NOT NULL, Primary Key ([BASKET_ID]));"
                + "Create table[WORKTIME]([WORKTIME_ID] Integer Identity NOT NULL, UNIQUE ([WORKTIME_ID]), [PLANTATION_ID] Integer NOT NULL,    [CODE_ID] Integer NOT NULL,    [DATE] Datetime NOT NULL,    [MINUTES] Integer NOT NULL,Primary Key([WORKTIME_ID]));"
                + "Create table[WORKER_CODES](    [CODE_ID] Integer Identity NOT NULL, UNIQUE ([CODE_ID]),    [WORKER_ID] Integer NOT NULL,    [CODE] Varchar(20) NOT NULL,    [PLANTATION_ID] Integer NOT NULL,Primary Key([CODE_ID]));"
                + "Create table[WORKERS](    [WORKER_ID] Integer Identity NOT NULL, UNIQUE ([WORKER_ID]),    [FIRST_NAME] Varchar(50) NOT NULL,    [LAST_NAME] Varchar(50) NULL,    [ADDRESS] Varchar(250) NULL,    [INSURANCE_INFO] Varchar(100) NULL,    [NIP] Varchar(10) NULL,    [PESEL] Varchar(11) NULL,    [REVENUE_OFFICE] Varchar(250) NULL,    [IS_ACTIVE] Bit Default 1 NOT NULL,Primary Key([WORKER_ID]));"
                + "Create table[PAYMENTS](    [PAYMENT_ID] Integer Identity NOT NULL, UNIQUE ([PAYMENT_ID]),    [PLANTATION_ID] Integer NOT NULL,    [FROM] Datetime NOT NULL,    [TO] Datetime NOT NULL,Primary Key([PAYMENT_ID]));"
                + "Create table[WORKER_PAYMENTS](    [PAYMENT_ID] Integer NOT NULL,    [WORKER_ID]        Integer NOT NULL,	[PREMIUM] Decimal(8,2) NOT NULL,Primary Key([PAYMENT_ID], [WORKER_ID]));"
                + "Create table[BASKET_PAY_RATES](    [PLANTATION_ID] Integer NOT NULL,    [DATE]        Datetime NOT NULL,	[RATE] Decimal(8,2) NULL);"
                + "Alter table[BASKETS] add foreign key([PLANTATION_ID]) references[PLANTATIONS]([PLANTATION_ID])  on update no action on delete no action;"
                + "Alter table[WORKTIME] add foreign key([PLANTATION_ID]) references[PLANTATIONS]([PLANTATION_ID])  on update no action on delete no action;"
                + "Alter table[PAYMENTS] add foreign key([PLANTATION_ID]) references[PLANTATIONS]([PLANTATION_ID])  on update no action on delete no action;"
                + "Alter table[BASKETS] add foreign key([CODE_ID]) references[WORKER_CODES]([CODE_ID])  on update no action on delete no action;"
                + "Alter table[WORKTIME] add foreign key([CODE_ID]) references[WORKER_CODES]([CODE_ID])  on update no action on delete no action;"
                + "Alter table[WORKER_CODES] add foreign key([WORKER_ID]) references[WORKERS]([WORKER_ID])  on update no action on delete no action;"
                + "Alter table[WORKER_PAYMENTS] add foreign key([WORKER_ID]) references[WORKERS]([WORKER_ID])  on update no action on delete no action;"
                + "Alter table[WORKER_PAYMENTS] add foreign key([PAYMENT_ID]) references[PAYMENTS]([PAYMENT_ID])  on update no action on delete no action;"
                + "Alter table[BASKET_PAY_RATES] add foreign key([PLANTATION_ID]) references[PLANTATIONS]([PLANTATION_ID])  on update no action on delete no action;"
                + "Alter table[WORKER_CODES] add constraint ucCodes unique([PLANTATION_ID], [CODE]);"
                + "Alter table[BASKET_PAY_RATES] add constraint ucBasketDailyRate unique([PLANTATION_ID], [DATE]);";
                using (var command = new SqlCommand(cmdText, connection))
                {
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }

        private static bool CheckDatabaseExists()
        {
            using (var connection = new SqlConnection("server=(local)\\SQLEXPRESS;Trusted_Connection=yes"))
            {
                connection.Open();
                using (var command = new SqlCommand($"SELECT db_id('{DATABASE_NAME}')", connection))
                {
                    return (command.ExecuteScalar() != DBNull.Value);
                }
            }
        }
    }
}
