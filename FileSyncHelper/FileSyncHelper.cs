﻿using OpenNETCF.Desktop.Communication;
using System;
using System.IO;
using System.Linq;

namespace BerrieSummary
{
    public static class FileSyncHelper
    {
        private static RAPI m_Device;
        private static ActiveSync m_ActiveSync;

        public const string LOCAL_DATA_DIRECTORY_PATH = "data\\";
        private const string DEVICE_DATA_FILES_PATH = "\\Program Files\\BerrieScanner\\data\\";
        private const string DEVICE_DATA_FILES_PATTERN = "*.bsdat";
        public const string WORKERS_1_FILE_NAME = "workers_1.bsdat";
        public const string WORKERS_2_FILE_NAME = "workers_2.bsdat";

        public delegate void FilesImported(object sender, EventArgs e);
        public static event FilesImported OnFilesImported;

        public delegate void FilesExported(object sender, EventArgs e);
        public static event FilesExported OnFilesExported;

        static FileSyncHelper()
        {
            EnsureDataDirectory();
            m_Device = new RAPI();
            m_ActiveSync = m_Device.ActiveSync;
        }

        public static void StartSync()
        {
            m_ActiveSync.Active += ActiveSync_Active;
            m_Device.RAPIConnected += Device_RAPIConnected;

            if (m_Device.DevicePresent)
            {
                m_Device.Connect();
            }
        }

        public static void StopSync()
        {
            if(m_Device.Connected)
            {
                m_Device.Disconnect();
                m_ActiveSync.Active -= ActiveSync_Active;
                m_Device.RAPIConnected -= Device_RAPIConnected;
            }
        }

        private static void Device_RAPIConnected()
        {
            MoveDataFiles();
        }

        public static void PushDictionaryFiles()
        {
            if (m_Device.Connected)
            {
                if (!m_Device.DeviceFileExists(DEVICE_DATA_FILES_PATH))
                {
                    m_Device.CreateDeviceDirectory(DEVICE_DATA_FILES_PATH);
                }
                // copy dictionary entries' files to device

                m_Device.CopyFileToDevice(
                    Path.Combine(LOCAL_DATA_DIRECTORY_PATH, WORKERS_1_FILE_NAME),
                    Path.Combine(DEVICE_DATA_FILES_PATH, WORKERS_1_FILE_NAME),
                    true);

                m_Device.CopyFileToDevice(
                    Path.Combine(LOCAL_DATA_DIRECTORY_PATH, WORKERS_2_FILE_NAME),
                    Path.Combine(DEVICE_DATA_FILES_PATH, WORKERS_2_FILE_NAME),
                    true);

                if (OnFilesExported != null)
                {
                    OnFilesExported(null, new EventArgs());
                }
            }
        }

        private static void MoveDataFiles()
        {
            bool raiseFilesImportedEvent = false;
            var dataFiles = m_Device.EnumerateFiles(Path.Combine(DEVICE_DATA_FILES_PATH, DEVICE_DATA_FILES_PATTERN));
            if (dataFiles.Any())
            {
                foreach (var dataFile in dataFiles)
                {
                    try
                    {
                        m_Device.CopyFileFromDevice(
                            Path.Combine(LOCAL_DATA_DIRECTORY_PATH, dataFile.FileName),
                            Path.Combine(DEVICE_DATA_FILES_PATH, dataFile.FileName),
                            true);
                    }
                    catch (RAPIException ex)
                    {
                        Console.WriteLine("[BS] RAPIException: {0}", ex.Message);
                    }

                    if (GetDataFileType(dataFile.FileName) == DataFileType.Baskets)
                    {
                        raiseFilesImportedEvent = true;
                        m_Device.DeleteDeviceFile(Path.Combine(DEVICE_DATA_FILES_PATH, dataFile.FileName));
                    }
                }

                if (raiseFilesImportedEvent && OnFilesImported != null)
                {
                    OnFilesImported(null, new EventArgs());
                }
            }
        }

        private static void ActiveSync_Active()
        {
            if (m_Device.DevicePresent)
            {
                m_Device.Connect();
            }
        }

        private static void EnsureDataDirectory()
        {
            var dir = Path.GetDirectoryName(LOCAL_DATA_DIRECTORY_PATH);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public static DataFileType GetDataFileType(string fileName)
        {
            if (fileName.StartsWith("baskets")) return DataFileType.Baskets;
            else if (fileName.StartsWith("workers")) return DataFileType.Workers;
            else return DataFileType.Unknown;
        }
    }
}
