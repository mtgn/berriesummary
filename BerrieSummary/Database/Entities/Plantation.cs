﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary
{
    public class Plantation
    {
        public int PlantationId { get; set; }
        public string Name { get; set; }
        public decimal Base_Rate { get; set; }
    }

    public sealed class PlantationsCache : IEnumerable<Plantation>
    {
        private static BindingList<Plantation> m_plantations = new BindingList<Plantation>();

        static PlantationsCache()
        {
            m_plantations = DatabaseHelper.GetPlantations();
        }

        public Plantation this[int id]
        {
            get
            {
                return m_plantations.FirstOrDefault(p => p.PlantationId == id);
            }
        }

        public IEnumerator<Plantation> GetEnumerator()
        {
            return m_plantations.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}