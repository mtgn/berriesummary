﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BerrieSummary
{
    public class WorkTime
    {
        public int WorkTimeId { get; set; }
        public int CodeId { get; set; }
        public int PlantationId { get; set; }
        public DateTime Date { get; set;}
        public int Minutes { get; set; }
    }
}
