﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;

namespace BerrieSummary.Payments
{
    public class PaymentsHelper
    {
        private int m_plantationId = -1;
        private PaymentsCache m_paymentsCache;

        public SortableBindingList<PaymentSummary> PaymentSummary { get; private set; }

        public PaymentsHelper(int plantationId)
        {
            m_plantationId = plantationId;
            CreatePaymentsList();
        }

        public void RefreshCache()
        {
            CreatePaymentsList();
        }

        private void CreatePaymentsList()
        {
            m_paymentsCache = new PaymentsCache(m_plantationId);

            var paymentSummaryList = new List<PaymentSummary>();
            foreach(var payment in m_paymentsCache.OrderByDescending(p=>p.To))
            {
                var payrollsHelper = new PayrollsHelper(payment.PaymentId);
                paymentSummaryList.Add(new PaymentSummary()
                {
                    PaymentId = payment.PaymentId,
                    DateFrom = payment.From,
                    DateTo = payment.To,
                    BasketsQuantity = payrollsHelper.Payrolls.Sum(p=>p.BasketsQuantity),
                    SumOfWorkers = payrollsHelper.Payrolls.Count,
                    TotalCost = payrollsHelper.Payrolls.Sum(p=>p.TotalValue)
                });
            }

            PaymentSummary = new SortableBindingList<PaymentSummary>(paymentSummaryList);
        }
    }
}