﻿using System;

namespace BerrieSummary
{
    public class BasketPayRate
    {
        public int PlantationId { get; set; }
        public DateTime Date { get; set; }
        public decimal? Rate { get; set; }
    }
}