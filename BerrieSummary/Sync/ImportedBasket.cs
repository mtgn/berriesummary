﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace BerrieSummary.Sync
{
    public class ImportedBasket : INotifyPropertyChanged
    {
        private bool m_isImported;
        private decimal m_fillingLevel;

        public bool IsImported
        {
            get
            {
                return m_isImported;
            }

            set
            {
                if (m_isImported != value)
                {
                    m_isImported = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int PlantationId { get; set; }
        public string WorkerFullName { get; set; }
        public decimal FillingLevel
        {
            get
            {
                return m_fillingLevel;
            }

            set
            {
                if(m_fillingLevel != value)
                {
                    m_fillingLevel = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Code { get; set; }
        public DateTime Date { get; set; }
        public string DateString
        {
            get
            {
                return Date.ToString("dd-MM-yyyy");
            }
        }

        public string FillingLevelString
        {
            get
            {
                return FillingLevel.ToString("0.0");
            }
            set
            {
                FillingLevel = decimal.Parse(value, CultureInfo.InvariantCulture);
            }
        }

        public string PlantationName
        {
            get
            {
                var plantation = new PlantationsCache()[PlantationId];
                if (plantation != null)
                {
                    return plantation.Name;
                }

                return null;
            }
        }
        public ImportedBasket()
        {
            IsImported = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged()
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(null));
            }
        }
    }
}
