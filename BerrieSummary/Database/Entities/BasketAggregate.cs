﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BerrieSummary
{
    public class BasketAggregate : IBasket
    {
        public BasketAggregate()
        {
        }

        public int BasketId { get; set; }
        public int CodeId { get; set; }
        public string Code { get; set; }
        public int PlantationId { get; set; }
        public string PlantationName { get; set; }
        public decimal Quantity { get; set; }
        public DateTime CollectionDate { get; set; }
        public string WorkerFullName { get; set; }
    }
}
