﻿namespace BerrieSummary
{
    public interface IWorker
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string FullName { get; }
    }
}