﻿using Simple.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using BerrieSummary.Sync;
using BerrieSummary.Payments;
using System.Data.SqlClient;
using System.IO;
using BerrieSummary.Properties;

namespace BerrieSummary
{
    public static class DatabaseHelper
    {
        public delegate void BackupProgress(object sender, BackupProgressEventArgs e);
        public static event BackupProgress OnBackupProgress;

        public static bool HasChanged;
        static DatabaseHelper()
        {
            HasChanged = false;
        }

        public static void CreateBackup()
        {
            // get backup path
            string backupDirectory = Settings.Default.BackupDirectory;
            string backupName = string.Format("Blueberries_db_{0:yyyyMMdd}.bak", DateTime.Today);
            string backupLogName = string.Format("Blueberries_log_{0:yyyyMMdd}.trn", DateTime.Today);
            var backupPath = Path.Combine(backupDirectory, backupName);
            var backupLogPath = Path.Combine(backupDirectory, backupLogName);

            // if backup already exists, delete it
            if (File.Exists(backupPath))
            {
                File.SetAttributes(backupPath, FileAttributes.Normal);
                File.Delete(backupPath);
            }

            if (File.Exists(backupLogPath))
            {
                File.SetAttributes(backupLogPath, FileAttributes.Normal);
                File.Delete(backupLogPath);
            }

            // if backups quantity has reached it's limit, delete the oldest
            var backups = Directory.GetFiles(backupDirectory, "*.bak");
            if (backups != null && backups.Count() >= Settings.Default.MaxBackups)
            {
                File.SetAttributes(backupPath, FileAttributes.Normal);
                File.Delete(backups.OrderBy(b => File.GetCreationTime(b)).FirstOrDefault());
            }

            var logBackups = Directory.GetFiles(backupDirectory, "*.trn");
            if (logBackups != null && logBackups.Count() >= Settings.Default.MaxBackups)
            {
                File.SetAttributes(backupLogPath, FileAttributes.Normal);
                File.Delete(logBackups.OrderBy(b => File.GetCreationTime(b)).FirstOrDefault());
            }

            // perform actual backup
            using (var connection = new SqlConnection("server=(local)\\SQLEXPRESS;Trusted_Connection=yes"))
            {
                connection.FireInfoMessageEventOnUserErrors = true;
                connection.InfoMessage += Connection_InfoMessage;
                connection.Open();
                using (var command = new SqlCommand($"BACKUP DATABASE [{Program.DATABASE_NAME}] TO  DISK = N'{backupPath}' WITH NOFORMAT, INIT, "
                    + "NAME = N'Blueberries-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10, CHECKSUM;", connection))
                {
                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand($"BACKUP LOG [{Program.DATABASE_NAME}] TO DISK = N'{backupLogPath}' WITH STATS = 10", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void CreateLogBackup()
        {
            // get backup path
            string backupDirectory = Settings.Default.BackupDirectory;
            string backupLogName = string.Format("Blueberries_log_{0:yyyyMMdd}.trn", DateTime.Today);
            var backupLogPath = Path.Combine(backupDirectory, backupLogName);

            // if backups quantity has reached it's limit, delete the oldest
            var logBackups = Directory.GetFiles(backupDirectory, "*.trn");
            if (logBackups != null && logBackups.Count() >= Settings.Default.MaxBackups)
            {
                File.SetAttributes(backupLogPath, FileAttributes.Normal);
                File.Delete(logBackups.OrderBy(b => File.GetCreationTime(b)).FirstOrDefault());
            }

            // perform actual backup
            using (var connection = new SqlConnection("server=(local)\\SQLEXPRESS;Trusted_Connection=yes"))
            {
                connection.FireInfoMessageEventOnUserErrors = true;
                connection.InfoMessage += Connection_InfoMessage;
                connection.Open();
                using (var command = new SqlCommand($"BACKUP LOG [{Program.DATABASE_NAME}] TO DISK = N'{backupLogPath}' WITH STATS = 10", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private static void Connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            foreach (SqlError info in e.Errors)
            {
                if (info.Class <= 10)
                {
                    var handler = OnBackupProgress;
                    if(handler != null)
                    {
                        int percent = 0;
                        int.TryParse(info.Message.Substring(0, info.Message.IndexOf(' ') + 1), out percent);
                        if (percent > 0)
                        {
                            handler(null, new BackupProgressEventArgs(percent));
                        }
                    }
                }
            }
        }

        public static Payment GetPayment(int paymentId)
        {
            var db = Database.Open();
            return db.Payments.Get(paymentId);
        }

        public static BindingList<Payment> GetPayments(int plantationId)
        {
            var db = Database.Open();
            List<Payment> paymentsRecords = db.Payments.FindAll(db.Payments.PlantationId == plantationId);
            return new BindingList<Payment>(paymentsRecords);
        }

        public static BindingList<WorkerPayment> GetWorkerPayments(int paymentId)
        {
            var db = Database.Open();
            List<WorkerPayment> workerPaymentsRecords = db.WorkerPayments.FindAll(db.WorkerPayments.PaymentId == paymentId);
            return new BindingList<WorkerPayment>(workerPaymentsRecords);
        }

        public static void UpdateBaseRate(Plantation plantation)
        {
            var db = Database.Open();
            db.Plantations.Update(plantation);
            HasChanged = true;
        }

        public static BindingList<BasketAggregate> GetBaskets(int plantationId)
        {
            var db = Database.Open();
            List<BasketAggregate> basketRecords = db.Baskets.FindAll(db.Baskets.PlantationId == plantationId);
            return new BindingList<BasketAggregate>(basketRecords);
        }

        public static DateTime? GetMinDate(int plantationId)
        {
            var db = Database.Open();
            DateTime? firstBasketDate = db.Baskets
                .FindAll(db.Baskets.PlantationId == plantationId)
                .Select(db.Baskets.CollectionDate.Min())
                .ToScalar();

            return firstBasketDate;
        }

        public static DateTime? GetMaxDate(int plantationId)
        {
            var db = Database.Open();
            DateTime? lastBasketDate = db.Baskets
                .FindAll(db.Baskets.PlantationId == plantationId)
                .Select(db.Baskets.CollectionDate.Max())
                .ToScalar();

            return lastBasketDate;
        }

        public static bool BasketRatesExist(int plantationId, DateTime dateFrom, DateTime dateTo)
        {
            var db = Database.Open();
            var datesCount = db.Baskets.All()
                .Select(db.Baskets.CollectionDate.Distinct().Count())
                .Where(db.Baskets.PlantationId == plantationId
                    && db.Baskets.CollectionDate >= dateFrom
                    && db.Baskets.CollectionDate <= dateTo)
                .ToScalar();

            var basketRatesCount = db.BasketPayRates.All()
                .Select(db.BasketPayRates.Date.Count())
                .Where(db.BasketPayRates.PlantationId == plantationId
                    && db.BasketPayRates.Date >= dateFrom
                    && db.BasketPayRates.Date <= dateTo
                    && db.BasketPayRates.Rate != null
                    && db.BasketPayRates.Rate > 0)
                .ToScalar();

            return datesCount == basketRatesCount;
        }

        public static void UpdatePayment(Payment payment)
        {
            var db = Database.Open();
            db.Payments.Update(payment);
            HasChanged = true;
        }

        public static int? InsertPayment(Payment payment)
        {
            var db = Database.Open();
            var paymentInserted = db.Payments.Insert(payment);
            HasChanged = true;
            return paymentInserted == null
                ? null
                : paymentInserted.PaymentId;
        }

        public static bool DeletePayment(int paymentId)
        {
            var db = Database.Open();
            var workerPaymentsDeleted = db.WorkerPayments.DeleteAll(db.WorkerPayments.PaymentId == paymentId);
            var paymentDeleted = db.Payments.DeleteByPaymentId(paymentId);
            HasChanged = true;
            return workerPaymentsDeleted != null && paymentDeleted != null;
        }

        public static BindingList<BasketPayRate> GetBasketRates(int plantationId)
        {
            var db = Database.Open();
            List<BasketPayRate> basketRateRecords = db.BasketPayRates.FindAll(db.BasketPayRates.PlantationId == plantationId);

            return new BindingList<BasketPayRate>(basketRateRecords);
        }

        public static DateTime? GetSuggestedNextPaymentDateFrom(int plantationId)
        {
            var db = Database.Open();
            DateTime? lastPaidDay = db.Payments
                .FindAll(db.Payments.PlantationId == plantationId)
                .Select(db.Payments.To.Max())
                .ToScalar();

            if (lastPaidDay.HasValue && lastPaidDay < GetMaxDate(plantationId).Value)
            {
                return lastPaidDay.Value.AddDays(1);
            }
            else if (lastPaidDay == null)
            {
                return GetMinDate(plantationId);
            }
            else
            {
                return (DateTime?)null;
            }
        }

        public static string GetCodeById(int codeId)
        {
            var db = Database.Open();
            return db.WorkerCodes.Get(codeId).Code;
        }

        public static BindingList<BasketAggregate> GetBaskets(int plantationId, int codeId)
        {
            var db = Database.Open();
            List<BasketAggregate> basketRecords = db.Baskets.FindAll(db.Baskets.PlantationId == plantationId && db.Baskets.CodeId == codeId);
            return new BindingList<BasketAggregate>(basketRecords);
        }

        public static void SetBasketRate(BasketPayRate basketPayRate)
        {
            var db = Database.Open();
            var basketRateRecord = db.BasketPayRates
                .Find(db.BasketPayRates.PlantationId == basketPayRate.PlantationId
                    && db.BasketPayRates.Date == basketPayRate.Date);

            if (basketRateRecord == null)
            {
                db.BasketPayRates.Insert(basketPayRate);
            }
            else
            {
                db.BasketPayRates.UpdateByPlantationIdAndDate(basketPayRate);
            }

            HasChanged = true;
        }

        public static void SetPremium(WorkerPayment workerPayment)
        {
            var db = Database.Open();
            var workerPaymentRecord = db.WorkerPayments
                .Find(db.WorkerPayments.PaymentId == workerPayment.PaymentId
                    && db.WorkerPayments.WorkerId == workerPayment.WorkerId);

            if (workerPaymentRecord == null && workerPayment.Premium != 0)
            {
                db.WorkerPayments.Insert(workerPayment);
            }
            else
            {
                if (workerPayment.Premium == 0)
                {
                    db.WorkerPayments.Delete(PaymentId: workerPayment.PaymentId, WorkerId: workerPayment.WorkerId);
                }
                else
                {
                    db.WorkerPayments.Update(workerPayment);
                }
            }

            HasChanged = true;
        }

        public static bool PaymentExists(int plantationId, DateTime date)
        {
            var db = Database.Open();
            var paymentsOnDate = db.Payments.Find(db.Payments.PlantationId == plantationId
                    && db.Payments.From <= date
                    && db.Payments.To >= date);

            return paymentsOnDate != null;
        }

        public static Worker GetWorkerByCodeId(int codeId)
        {
            var db = Database.Open();
            var workerCode = db.WorkerCodes.Get(codeId);
            if (workerCode == null)
            {
                return null;
            }

            return db.Workers.FindByWorkerId(workerCode.WorkerId);
        }

        public static bool InsertWorkTime(WorkTime workTime)
        {
            var db = Database.Open();
            var result = db.Worktime.Insert(new
            {
                CODE_ID = workTime.CodeId,
                PLANTATION_ID = workTime.PlantationId,
                DATE =  workTime.Date,
                MINUTES = workTime.Minutes
            });

            HasChanged = true;
            return result != null;
        }

        public static bool InsertBasket(BasketAggregate basket)
        {
            var db = Database.Open();
            var result = db.Baskets.Insert(new
            {
                CODE_ID = basket.CodeId,
                PLANTATION_ID = basket.PlantationId,
                COLLECTION_DATE = basket.CollectionDate,
                QUANTITY = basket.Quantity
            });

            HasChanged = true;
            return result != null;
        }

        public static BindingList<WorkTime> GetWorkTime(int plantationId)
        {
            var db = Database.Open();
            List<WorkTime> workTimeRecords = db.Worktime.FindAll(db.Worktime.PlantationId == plantationId);
            return new BindingList<WorkTime>(workTimeRecords);
        }

        public static BindingList<WorkTime> GetWorkTime(int plantationId, int codeId)
        {
            var db = Database.Open();
            List<WorkTime> workTimeRecords = db.Worktime.FindAll(db.Worktime.PlantationId == plantationId && db.Worktime.CodeId == codeId);
            return new BindingList<WorkTime>(workTimeRecords);
        }

        public static BindingList<WorkerCode> GetWorkerCodes()
        {
            var db = Database.Open();
            List<WorkerCode> workerCodeRecords = db.WorkerCodes.WithWorkers().Where(db.Workers.IsActive == true);
            return new BindingList<WorkerCode>(workerCodeRecords);
        }

        public static List<WorkPerCode> GetWorkPerCode(int plantationId, DateTime dateFrom, DateTime dateTo)
        {
            var db = Database.Open();
            List<WorkPerCode> workPerCode = db.WorkerCodes
                .WithBaskets()
                .Where(db.WorkerCodes.PlantationId == plantationId 
                    && db.WorkerCodes.Baskets.CollectionDate >= dateFrom
                    && db.WorkerCodes.Baskets.CollectionDate <= dateTo);

            return workPerCode.Where(x=>x.Baskets != null).ToList();
        }

        public static BindingList<WorkerCode> GetWorkerCodes(int plantationId)
        {
            var db = Database.Open();
            List<WorkerCode> workerCodeRecords = db.WorkerCodes.FindAll(db.WorkerCodes.PlantationId == plantationId);
            return new BindingList<WorkerCode>(workerCodeRecords);
        }

        public static WorkerCode GetWorkerCode(int plantationId, int workerId)
        {
            var db = Database.Open();
            var workerCode = db.WorkerCodes.Find(db.WorkerCodes.PlantationId == plantationId && db.WorkerCodes.WorkerId == workerId);
            return workerCode;
        }

        public static BindingList<Plantation> GetPlantations()
        {
            var db = Database.Open();
            List<Plantation> plantationRecords = db.Plantations.All();
            if(!plantationRecords.Any())
            {
                plantationRecords = CreatePlantations(db);
            }

            return new BindingList<Plantation>(plantationRecords);
        }

        private static List<Plantation> CreatePlantations(dynamic db)
        {
            var plantations = new[]
            {
                new Plantation() { PlantationId = 1, Name = "Wojnicz", Base_Rate = 9.00M },
                new Plantation() { PlantationId = 2, Name = "Wierzchosławice", Base_Rate = 9.00M },
            };

            HasChanged = true;
            return db.Plantations.Insert(plantations);
        }

        public static bool DeleteWorker(int workerId)
        {
            var db = Database.Open();
            return db.Workers.UpdateByWorkerId(WorkerId: workerId, IsActive: false) == 1;
        }

        public static bool InsertWorker(Worker worker, WorkerCode code)
        {
            var db = Database.Open();
            var insertWorkerResult = db.Workers.Insert(worker);
            if (insertWorkerResult != null)
            {
                code.WorkerId = insertWorkerResult.WorkerId;
                var insertWorkerCodeResult = db.WorkerCodes.Insert(code);
                if (insertWorkerCodeResult != null)
                {
                    HasChanged = true;
                    return true;
                }
            }

            return false;
        }

        public static BindingList<Worker> GetWorkers()
        {
            var db = Database.Open();
            List<Worker> workerRecords = db.Workers.FindAll(db.Workers.IsActive == true);
            return new BindingList<Worker>(workerRecords);
        }

        public static bool UpdateWorker(Worker worker)
        {
            var db = Database.Open();
            HasChanged = true;
            return db.Workers.UpdateByWorkerId(worker) == 1;
        }

        public static BindingList<Worker> GetWorkers(int plantationId)
        {
            var db = Database.Open();
            List<CodeWorker> codeWorkers = db.WorkerCodes.FindAll(db.WorkerCodes.PlantationId == plantationId)
                .WithWorkers();
            var workerRecords = codeWorkers.Select(x => x.Workers)
                .Distinct(new ByIdEqualityComparer())
                .Where(w=>w.IsActive).ToList();
            return new BindingList<Worker>(workerRecords);
        }

        public static Worker GetWorkerById(int id)
        {
            var db = Database.Open();
            return db.Workers.Find(db.Workers.WorkerId == id);
        }

        public static Worker GetWorkerByCode(string code)
        {
            var db = Database.Open();
            var workerCode = db.WorkerCodes.FindByCode(code);
            if (workerCode == null)
            {
                return null;
            }
            
            return db.Workers.Find(workerCode.WorkerId);
        }

        public static bool InsertImportedWorkers(IEnumerable<ImportedWorker> importedWorkers)
        {
            var codesCache = new WorkerCodesCache();
            int workersInserted = 0;
            int workerCodesInserted = 0;

            var db = Database.Open();
            foreach (var worker in importedWorkers)
            {
                if (codesCache[worker.Code, worker.PlantationId] == null)
                {
                    var workerRecord = new
                    {
                        FIRST_NAME = worker.FirstName,
                        LAST_NAME = worker.LastName
                    };
                    var workerInserted = db.Workers.Insert(workerRecord);
                    workersInserted++;

                    var workerCodeRecord = new
                    {
                        WORKER_ID = workerInserted.WorkerId,
                        PLANTATION_ID = worker.PlantationId,
                        CODE = worker.Code
                    };
                    db.WorkerCodes.Insert(workerCodeRecord);
                    workerCodesInserted++;
                }
                else
                {
                    Console.WriteLine("[BS] WorkerCode already exists for (CODE={0},PLANTATION_ID={1}).", worker.Code, worker.PlantationId);
                }
            }

            HasChanged = true;
            return workersInserted == workerCodesInserted && workersInserted == importedWorkers.Count();
        }

        public static bool InsertImportedBaskets(IEnumerable<BasketAggregate> importedBaskets)
        {
            var codesCache = new WorkerCodesCache();
            var basketRecordList = new List<object>();
            var db = Database.Open();
            foreach (var basket in importedBaskets)
            {
                var basketRecord = new
                {
                    CODE_ID = codesCache[basket.Code, basket.PlantationId].CodeId,
                    PLANTATION_ID = basket.PlantationId,
                    QUANTITY = basket.Quantity,
                    COLLECTION_DATE = basket.CollectionDate
                };
                basketRecordList.Add(basketRecord);
            }

            HasChanged = true;
            var basketsInserted = db.Baskets.Insert(basketRecordList);
            return basketsInserted != null && basketsInserted.ToList().Count == importedBaskets.Count();
        }

        public static bool IsCodeAvailable(string code, int plantationId)
        {
            var db = Database.Open();
            var workerCode = db.WorkerCodes
                .Find(db.WorkerCodes.PlantationId == plantationId && db.WorkerCodes.Code == code);

            return workerCode == null;
        }

        public static bool InsertImportedWorkTime(IEnumerable<ImportedWorkTime> importedWorkTime)
        {
            var codesCache = new WorkerCodesCache();
            var workTimeRecordList = new List<object>();
            var db = Database.Open();
            foreach(var workTime in importedWorkTime)
            {
                var workerCode = codesCache[workTime.Code, workTime.PlantationId];
                if (workerCode != null)
                {
                    var workTimeRecord = new
                    {
                        CODE_ID = workerCode.CodeId,
                        PLANTATION_ID = workTime.PlantationId,
                        DATE = workTime.From.Date,
                        MINUTES = workTime.TotalMinutes
                    };
                    workTimeRecordList.Add(workTimeRecord);
                }
                else
                {
                    Console.WriteLine("[BS] WorkerCode not found for (CODE={0},PLANTATION_ID={1}).", workTime.Code, workTime.PlantationId);
                }
            }

            HasChanged = true;
            var workTimeInserted = db.Worktime.Insert(workTimeRecordList);
            return workTimeInserted != null;
        }

        private class CodeWorker
        {
            public Worker Workers { get; set; }

            public CodeWorker()
            {
            }
        }
    }
}