﻿using BerrieSummary.Harvests;
using System;
using System.Linq;

namespace BerrieSummary.Workers
{
    public class WorkerDailySummary
    {
        public int PlantationId { get; set; }
        public DateTime Date { get; set; }
        public decimal BasketsQuantity { get; set; }
        public int WorkingMinutes { get; set; }
        public decimal? BasketRate { get; set; }
        public decimal? TotalValue
        {
            get
            {
                return BasketRate.HasValue 
                    ? BasketRate * BasketsQuantity
                    : null;
            }
        }

        public string DateString
        {
            get
            {
                return Date.ToString("dd-MM-yyyy");
            }
        }

        public string BasketsQuantityString
        {
            get
            {
                return BasketsQuantity.ToString("0.00");
            }
        }

        public string WorkTimeString
        {
            get
            {
                return WorkingMinutes.FromMinutesToTimeString();
            }
        }

        public double? Tempo
        {
            get
            {
                return WorkingMinutes == 0
                    ? (double?)null
                    : (double)BasketsQuantity / (WorkingMinutes / 60);
            }
        }

        public double? Efficiency
        {
            get
            {
                var harvestsHelper = new HarvestsHelper(PlantationId);
                var harvestSummary = harvestsHelper.HarvestsSummary.FirstOrDefault(s => s.DateOfHarvest == Date);
                return harvestSummary != null && harvestSummary.AverageTempo.HasValue && Tempo.HasValue
                    ? Tempo.Value / harvestSummary.AverageTempo.Value
                    : (double?)null;
            }
        }

        public string EfficiencyString
        {
            get
            {
                return Efficiency.HasValue
                    ? Efficiency.Value.ToString("0.00")
                    : string.Empty;
            }
        }

        public string BasketRateString
        {
            get
            {
                return BasketRate.HasValue 
                    ? BasketRate.Value.ToString("0.00 zł")
                    : string.Empty;
            }
        }

        public string HourRateString
        {
            get
            {
                return TotalValue.HasValue && WorkingMinutes > 0
                    ? (TotalValue.Value / (WorkingMinutes / 60)).ToString("0.00 zł")
                    : string.Empty;
            }
        }

        public string TotalValueString
        {
            get
            {
                return TotalValue.HasValue
                    ? TotalValue.Value.ToString("0.00 zł")
                    : string.Empty;
            }
        }
    }
}