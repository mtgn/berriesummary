﻿namespace BerrieSummary.Payments
{
    partial class PaymentsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentsForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PaymentsGrid = new System.Windows.Forms.DataGridView();
            this.pPaymentId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pDateFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pDateTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pBasketsQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pSumOfWorkers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pTotalCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ChangeBaseRateButton = new System.Windows.Forms.Button();
            this.DeletePayrollButton = new System.Windows.Forms.Button();
            this.PreviewPayrollButton = new System.Windows.Forms.Button();
            this.EditPayrollButton = new System.Windows.Forms.Button();
            this.AddPayrollButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PaymentsGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.Controls.Add(this.PaymentsGrid, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(777, 451);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // PaymentsGrid
            // 
            this.PaymentsGrid.AllowUserToAddRows = false;
            this.PaymentsGrid.AllowUserToDeleteRows = false;
            this.PaymentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PaymentsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.pPaymentId,
            this.pDateFrom,
            this.pDateTo,
            this.pBasketsQuantity,
            this.pSumOfWorkers,
            this.pTotalCost});
            this.PaymentsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PaymentsGrid.Location = new System.Drawing.Point(3, 3);
            this.PaymentsGrid.MultiSelect = false;
            this.PaymentsGrid.Name = "PaymentsGrid";
            this.PaymentsGrid.ReadOnly = true;
            this.PaymentsGrid.RowHeadersVisible = false;
            this.PaymentsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PaymentsGrid.Size = new System.Drawing.Size(529, 445);
            this.PaymentsGrid.TabIndex = 1;
            // 
            // pPaymentId
            // 
            this.pPaymentId.DataPropertyName = "PaymentId";
            this.pPaymentId.HeaderText = "ID";
            this.pPaymentId.Name = "pPaymentId";
            this.pPaymentId.ReadOnly = true;
            this.pPaymentId.Visible = false;
            // 
            // pDateFrom
            // 
            this.pDateFrom.DataPropertyName = "DateFromString";
            this.pDateFrom.HeaderText = "Data od";
            this.pDateFrom.Name = "pDateFrom";
            this.pDateFrom.ReadOnly = true;
            // 
            // pDateTo
            // 
            this.pDateTo.DataPropertyName = "DateToString";
            this.pDateTo.HeaderText = "Data do";
            this.pDateTo.Name = "pDateTo";
            this.pDateTo.ReadOnly = true;
            // 
            // pBasketsQuantity
            // 
            this.pBasketsQuantity.DataPropertyName = "BasketsQuantityString";
            this.pBasketsQuantity.HeaderText = "Suma koszyków";
            this.pBasketsQuantity.Name = "pBasketsQuantity";
            this.pBasketsQuantity.ReadOnly = true;
            // 
            // pSumOfWorkers
            // 
            this.pSumOfWorkers.DataPropertyName = "SumOfWorkers";
            this.pSumOfWorkers.HeaderText = "Suma pracowników";
            this.pSumOfWorkers.Name = "pSumOfWorkers";
            this.pSumOfWorkers.ReadOnly = true;
            // 
            // pTotalCost
            // 
            this.pTotalCost.DataPropertyName = "TotalCostString";
            this.pTotalCost.HeaderText = "Łączny koszt";
            this.pTotalCost.Name = "pTotalCost";
            this.pTotalCost.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ChangeBaseRateButton);
            this.panel1.Controls.Add(this.DeletePayrollButton);
            this.panel1.Controls.Add(this.PreviewPayrollButton);
            this.panel1.Controls.Add(this.EditPayrollButton);
            this.panel1.Controls.Add(this.AddPayrollButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(538, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 445);
            this.panel1.TabIndex = 2;
            // 
            // ChangeBaseRateButton
            // 
            this.ChangeBaseRateButton.BackColor = System.Drawing.Color.AliceBlue;
            this.ChangeBaseRateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChangeBaseRateButton.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ChangeBaseRateButton.ForeColor = System.Drawing.Color.Black;
            this.ChangeBaseRateButton.Location = new System.Drawing.Point(33, 380);
            this.ChangeBaseRateButton.Name = "ChangeBaseRateButton";
            this.ChangeBaseRateButton.Size = new System.Drawing.Size(164, 45);
            this.ChangeBaseRateButton.TabIndex = 14;
            this.ChangeBaseRateButton.Text = "Zmień stawkę godzinową";
            this.ChangeBaseRateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.ChangeBaseRateButton.UseVisualStyleBackColor = false;
            this.ChangeBaseRateButton.Click += new System.EventHandler(this.ChangeBaseRateButton_Click);
            // 
            // DeletePayrollButton
            // 
            this.DeletePayrollButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.DeletePayrollButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeletePayrollButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DeletePayrollButton.ForeColor = System.Drawing.Color.Black;
            this.DeletePayrollButton.Location = new System.Drawing.Point(33, 310);
            this.DeletePayrollButton.Name = "DeletePayrollButton";
            this.DeletePayrollButton.Size = new System.Drawing.Size(164, 45);
            this.DeletePayrollButton.TabIndex = 13;
            this.DeletePayrollButton.Text = "Usuń";
            this.DeletePayrollButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.DeletePayrollButton.UseVisualStyleBackColor = false;
            this.DeletePayrollButton.Click += new System.EventHandler(this.DeletePayrollButton_Click);
            // 
            // PreviewPayrollButton
            // 
            this.PreviewPayrollButton.BackColor = System.Drawing.Color.Orange;
            this.PreviewPayrollButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PreviewPayrollButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PreviewPayrollButton.ForeColor = System.Drawing.Color.Black;
            this.PreviewPayrollButton.Location = new System.Drawing.Point(33, 37);
            this.PreviewPayrollButton.Name = "PreviewPayrollButton";
            this.PreviewPayrollButton.Size = new System.Drawing.Size(164, 74);
            this.PreviewPayrollButton.TabIndex = 12;
            this.PreviewPayrollButton.Text = "Pokaż\r\nlistę płac";
            this.PreviewPayrollButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PreviewPayrollButton.UseVisualStyleBackColor = false;
            this.PreviewPayrollButton.Click += new System.EventHandler(this.PreviewPayrollButton_Click);
            // 
            // EditPayrollButton
            // 
            this.EditPayrollButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.EditPayrollButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditPayrollButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditPayrollButton.ForeColor = System.Drawing.Color.Black;
            this.EditPayrollButton.Location = new System.Drawing.Point(33, 239);
            this.EditPayrollButton.Name = "EditPayrollButton";
            this.EditPayrollButton.Size = new System.Drawing.Size(164, 45);
            this.EditPayrollButton.TabIndex = 11;
            this.EditPayrollButton.Text = "Edytuj";
            this.EditPayrollButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.EditPayrollButton.UseVisualStyleBackColor = false;
            this.EditPayrollButton.Click += new System.EventHandler(this.EditPayrollButton_Click);
            // 
            // AddPayrollButton
            // 
            this.AddPayrollButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.AddPayrollButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddPayrollButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddPayrollButton.ForeColor = System.Drawing.Color.Black;
            this.AddPayrollButton.Location = new System.Drawing.Point(33, 139);
            this.AddPayrollButton.Name = "AddPayrollButton";
            this.AddPayrollButton.Size = new System.Drawing.Size(164, 72);
            this.AddPayrollButton.TabIndex = 10;
            this.AddPayrollButton.Text = "Przygotuj wypłatę";
            this.AddPayrollButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AddPayrollButton.UseVisualStyleBackColor = false;
            this.AddPayrollButton.Click += new System.EventHandler(this.AddPayrollButton_Click);
            // 
            // PaymentsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 451);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PaymentsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Wypłaty";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PaymentsGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView PaymentsGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button DeletePayrollButton;
        private System.Windows.Forms.Button PreviewPayrollButton;
        private System.Windows.Forms.Button EditPayrollButton;
        private System.Windows.Forms.Button AddPayrollButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn pPaymentId;
        private System.Windows.Forms.DataGridViewTextBoxColumn pDateFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn pDateTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn pBasketsQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn pSumOfWorkers;
        private System.Windows.Forms.DataGridViewTextBoxColumn pTotalCost;
        private System.Windows.Forms.Button ChangeBaseRateButton;
    }
}