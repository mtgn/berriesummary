﻿using CarlosAg.ExcelXmlWriter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary.Payments
{
    public partial class PayrollForm : Form
    {
        private int m_paymentId;
        private PayrollsHelper m_payrollsHelper;

        private PayrollForm()
        {
            InitializeComponent();
        }

        public PayrollForm(int paymentId)
            :this()
        {
            m_paymentId = paymentId;
            m_payrollsHelper = new PayrollsHelper(m_paymentId);

            DateFromLabel.Text = m_payrollsHelper.Payment.From.ToString("dd-MM-yyyy");
            DateToLabel.Text = m_payrollsHelper.Payment.To.ToString("dd-MM-yyyy");
            RefreshTotalAmountLabel();

            RefreshGrid();
        }

        private void RefreshTotalAmountLabel()
        {
            TotalAmountLabel.Text = m_payrollsHelper.Payrolls.Sum(p => p.TotalValue).ToString("# ##0 zł");
        }

        private void RefreshGrid()
        {
            m_payrollsHelper.RefreshCache();
            PayrollsGrid.AutoGenerateColumns = false;
            PayrollsGrid.DataSource = null;
            PayrollsGrid.DataSource = m_payrollsHelper.Payrolls;
        }

        private void PayrollsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void PayrollsGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != wpPremium.Index)
            {
                return;
            }

            var value = e.FormattedValue.ToString();

            var indexOfCurrencySymbol = value.IndexOf(" zł");
            if (indexOfCurrencySymbol >= 0)
            {
                value = value.Remove(indexOfCurrencySymbol);
            }

            if (!string.IsNullOrEmpty(value))
            {
                decimal result = 0;
                if (!decimal.TryParse(value, out result))
                {
                    MessageBox.Show("Nieprawidłowa wartość premii.");
                    e.Cancel = true;
                }
            }
        }

        private void PayrollsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (PayrollsGrid.IsCurrentCellInEditMode && e.ColumnIndex == wpPremium.Index)
            {
                var payroll = PayrollsGrid.Rows[e.RowIndex].DataBoundItem as Payroll;
                var workerPayment = payroll.GetWorkerPayment();
                DatabaseHelper.SetPremium(workerPayment);
                PayrollsGrid.InvalidateCell(wpTotalValue.Index, e.RowIndex);
                RefreshTotalAmountLabel();
            }
        }

        private void NominalValuesButton_Click(object sender, EventArgs e)
        {
            int hundreads = 0, fifties = 0, twenties = 0, tens = 0, fives = 0, twos = 0, ones = 0;

            foreach (var payroll in m_payrollsHelper.Payrolls)
            {
                int remainder;
                hundreads += Math.DivRem((int)payroll.TotalValue, 100, out remainder);
                fifties += Math.DivRem(remainder, 50, out remainder);
                twenties += Math.DivRem(remainder, 20, out remainder);
                tens += Math.DivRem(remainder, 10, out remainder);
                fives += Math.DivRem(remainder, 5, out remainder);
                twos += Math.DivRem(remainder, 2, out remainder);
                ones += Math.DivRem(remainder, 1, out remainder);

                if (remainder != 0) return;
            }

            var sb = new StringBuilder("Potrzebne nominały:");
            if (hundreads > 0) sb.AppendFormat("\n{0}x   100 zł", hundreads);
            if (fifties > 0) sb.AppendFormat("\n{0}x   50 zł", fifties);
            if (twenties > 0) sb.AppendFormat("\n{0}x   20 zł", twenties);
            if (tens > 0) sb.AppendFormat("\n{0}x   10 zł", tens);
            if (fives > 0) sb.AppendFormat("\n{0}x   5 zł", fives);
            if (twos > 0) sb.AppendFormat("\n{0}x   2 zł", twos);
            if (ones > 0) sb.AppendFormat("\n{0}x   1 zł", ones);

            MessageBox.Show(sb.ToString(), "Nominały", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #region Printing

        private void PrintButton_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = string.Format("{2}, od {0:dd-MM-yyyy} do {1:dd-MM-yyyy}",
                m_payrollsHelper.Payment.From, m_payrollsHelper.Payment.To, new PlantationsCache()[m_payrollsHelper.Payment.PlantationId].Name);
            saveFileDialog1.ShowDialog();
        }

        private void PrintPaymentList(string fileName)
        {
            var book = new Workbook();
            book.SpreadSheetComponentOptions.Toolbar.Hidden = true;

            var sheet = book.Worksheets.Add("Lista płac");
            sheet.Protected = true;
            sheet.Options.PageSetup.Layout.Orientation = CarlosAg.ExcelXmlWriter.Orientation.Landscape;
            sheet.Options.PageSetup.PageMargins.Top = 0.25F;
            sheet.Options.PageSetup.PageMargins.Bottom = 0.25F;
            sheet.Options.PageSetup.PageMargins.Left = 0.35F;
            sheet.Options.PageSetup.PageMargins.Right = 0.35F;
            sheet.Options.Print.GridLines = true;

            CreateHeaders(book, sheet);

            var indexStyle = book.Styles.Add("IndexStyle");
            indexStyle.Alignment.WrapText = true;
            indexStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            indexStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            indexStyle.Font.FontName = "Segoe UI";

            var stringStyle = book.Styles.Add("StringStyle");
            stringStyle.Alignment.WrapText = true;
            stringStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            stringStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            stringStyle.Font.FontName = "Segoe UI";

            var codeStyle = book.Styles.Add("CodeStyle");
            codeStyle.Alignment.WrapText = true;
            codeStyle.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            codeStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            codeStyle.Font.FontName = "Segoe UI";

            var moneyStyle = book.Styles.Add("MoneyStyle");
            moneyStyle.Alignment.WrapText = true;
            moneyStyle.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            moneyStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            moneyStyle.Font.FontName = "Segoe UI";

            var totalValueStyle = book.Styles.Add("TotalValueStyle");
            totalValueStyle.Alignment.WrapText = true;
            totalValueStyle.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            totalValueStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            totalValueStyle.Font.FontName = "Segoe UI";
            totalValueStyle.Font.Bold = true;
            

            for (int rowIndex = 0, rowCount = PayrollsGrid.Rows.Count; rowIndex < rowCount; ++rowIndex)
            {
                var row = sheet.Table.Rows.Add();
                row.Height = 30;

                var payroll = PayrollsGrid.Rows[rowIndex].DataBoundItem as Payroll;

                row.Cells.Add(string.Format("{0}.", rowIndex + 1), DataType.String, indexStyle.ID);
                row.Cells.Add(payroll.Code, DataType.String, codeStyle.ID);
                row.Cells.Add(payroll.FullName, DataType.String, stringStyle.ID);
                row.Cells.Add(payroll.BasketsQuantityString, DataType.String, stringStyle.ID);
                row.Cells.Add(payroll.BasketRateString, DataType.String, moneyStyle.ID);
                row.Cells.Add(payroll.WorkValueString, DataType.String, moneyStyle.ID);
                row.Cells.Add(payroll.PremiumString, DataType.String, moneyStyle.ID);
                row.Cells.Add(payroll.TotalValueString, DataType.String, totalValueStyle.ID);
                row.Cells.Add();
            }

            try
            {
                book.Save(fileName);
            }
            catch(Exception)
            {
                MessageBox.Show("Nie udało się zapisać listy płac. Spróbuj zmienić katalog lub nazwę pliku.");
            }
        }

        private void CreateHeaders(Workbook book, Worksheet sheet)
        {
            var titleStyle = book.Styles.Add("TitleStyle");
            titleStyle.Alignment.WrapText = true;
            titleStyle.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            titleStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            titleStyle.Font.FontName = "Segoe UI";

            var headerStyle = book.Styles.Add("HeaderStyle");
            headerStyle.Alignment.WrapText = true;
            headerStyle.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            headerStyle.Alignment.Vertical = StyleVerticalAlignment.Center;
            headerStyle.Font.FontName = "Segoe UI";
            headerStyle.Font.Color = "#aaaaaa";

            var indexCol = sheet.Table.Columns.Add(25);
            var codeCol = sheet.Table.Columns.Add(50);
            var nameCol = sheet.Table.Columns.Add(150);
            var basketCountCol = sheet.Table.Columns.Add(50);
            var avgRateCol = sheet.Table.Columns.Add(60);
            var workValueCol = sheet.Table.Columns.Add(60);
            var premiumCol = sheet.Table.Columns.Add(60);
            var totalValueCol = sheet.Table.Columns.Add(60);
            var signatureCol = sheet.Table.Columns.Add(160);

            var titleRow = sheet.Table.Rows.Add();
            titleRow.Height = 30;
            var titleCell = titleRow.Cells.Add(string.Format("Lista płac, {2}, od {0:dd-MM-yyyy} do {1:dd-MM-yyyy}", 
                m_payrollsHelper.Payment.From, m_payrollsHelper.Payment.To, new PlantationsCache()[m_payrollsHelper.Payment.PlantationId].Name));
            titleCell.MergeAcross = 8;
            titleCell.StyleID = titleStyle.ID;

            var colHeadersRow = sheet.Table.Rows.Add();
            colHeadersRow.Height = 50;
            colHeadersRow.Cells.Add("Lp.", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Kod", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Imię i nazwisko", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Suma koszyków", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Średnia stawka za koszyk", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Do zapłaty", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Premia", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("RAZEM", DataType.String, headerStyle.ID);
            colHeadersRow.Cells.Add("Podpis", DataType.String, headerStyle.ID);
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            PrintPaymentList(saveFileDialog1.FileName);
            if (File.Exists(saveFileDialog1.FileName))
            {
                Process.Start(saveFileDialog1.FileName);
            }
        }

        #endregion
    }
}
