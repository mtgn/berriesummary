﻿namespace BerrieSummary.Sync
{
    public class WorkerCodePair
    {
        public int PlantationId { get; set; }
        public string PlantationName
        {
            get
            {
                return new PlantationsCache()[PlantationId].Name;
            }
        }
        public string OldCode { get; set; }
        public string NewCode { get; set; }
    }
}