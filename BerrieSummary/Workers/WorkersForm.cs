﻿using BerrieSummary.Sync;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary.Workers
{
    public partial class WorkersForm : Form
    {
        private int m_plantationId = -1;
        private WorkersHelper m_workersHelper;
        private WorkerCodePair m_missingCode;

        public int SelectedWorkerId
        {
            get
            {
                if (WorkersGrid.SelectedRows.Count == 0)
                {
                    return -1;
                }

                var row = WorkersGrid.SelectedRows[0];
                return (int)row.Cells[0].Value;
            }
        }

        private WorkersForm()
        {
            InitializeComponent();
            WorkersGrid.AutoGenerateColumns = false;
        }

        public WorkersForm(int plantationId, WorkerCodePair missingCode = null)
            :this()
        {
            m_plantationId = plantationId;
            m_missingCode = missingCode;
            m_workersHelper = new WorkersHelper(plantationId);
            WorkersGrid.DataSource = m_workersHelper.Workers;

            if(missingCode != null)
            {
                PickWorkerButton.Visible = true;
                EditWorkerButton.Visible = false;
                DeleteWorkerButton.Visible = false;
            }
        }

        private void PreviewWorkerButton_Click(object sender, EventArgs e)
        {
            if (SelectedWorkerId > 0)
            {
                using (var form = new WorkerDetailsForm(m_plantationId, SelectedWorkerId, true))
                {
                    form.ShowDialog();
                }
            }
        }

        private void AddWorkerButton_Click(object sender, EventArgs e)
        {
            using (var form = new WorkerDetailsForm(m_plantationId, m_missingCode == null ? null : m_missingCode.OldCode))
            {
                var result = form.ShowDialog();
                if(result == DialogResult.No)
                {
                    MessageBox.Show("Nie udało się dodać pracownika.", "Dodawanie pracownika", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if(result == DialogResult.OK)
                {
                    RefreshGrid();
                }
            }
        }

        private void EditWorkerButton_Click(object sender, EventArgs e)
        {
            if (SelectedWorkerId > 0)
            {
                using (var form = new WorkerDetailsForm(m_plantationId, SelectedWorkerId, false))
                {
                    var result = form.ShowDialog();
                    if (result == DialogResult.No)
                    {
                        MessageBox.Show("Nie udało się edytować pracownika.", "Edycja pracownika", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if(result == DialogResult.OK)
                    {
                        RefreshGrid();
                    }
                }
            }
        }

        private void DeleteWorkerButton_Click(object sender, EventArgs e)
        {
            if (SelectedWorkerId > 0)
            {
                if (MessageBox.Show("Czy na pewno chcesz usunąć wybranego pracownika?",
                    "Usuwanie pracownika", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (DatabaseHelper.DeleteWorker(SelectedWorkerId))
                    {
                        RefreshGrid();
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się usunąć pracownika.", "Usuwanie pracownika", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void RefreshGrid()
        {
            m_workersHelper.RefreshCache();
            WorkersGrid.DataSource = null;
            WorkersGrid.DataSource = m_workersHelper.Workers;
        }

        private void PickWorkerButton_Click(object sender, EventArgs e)
        {
            if (m_missingCode != null && SelectedWorkerId > 0)
            {
                m_missingCode.NewCode = DatabaseHelper.GetWorkerCode(m_plantationId, SelectedWorkerId).Code;
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void WorkerHistoryButton_Click(object sender, EventArgs e)
        {
            if (SelectedWorkerId > 0)
            {
                var code = DatabaseHelper.GetWorkerCode(m_plantationId, SelectedWorkerId);
                using (var form = new WorkerHistoryForm(m_plantationId, code.CodeId))
                {
                    form.ShowDialog();
                }
            }
        }
    }
}
