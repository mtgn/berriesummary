skaner:
efektywność -> dodać kolumnę ilość koszyków w poprzednich dniach +
wyświetlanie klawiatury +


kolorowanie aktywnych pracowników
kolorowanie kolumny % napełnienia
skrót klawiszowy do usuwania koszyków
menu jako główny ekran
szybsze usuwanie koszyka -> obok przycisku menu dodać mniejszy przycisk "usuń" (za potwierdzeniem)
usuwanie wszystkich koszyków
edycja imienia i nazwiska pracownika
kolumna kod mniejsza na rzecz kolumny imię i nazwisko
% 1.0 jako 1


komputer:

lista pracowników:
przycisk: historia zbiorów -> data, ilość koszyków, liczba godzin, stawka za koszyk, stawka za godzinę, razem za dzień; ręczne dodawanie liczby koszyków oraz liczby godzin +
wypłaty:
lista płac: kod pracownika, imię i nazwisko, łączna ilość zebranych koszyków, średnia stawka zł za koszyk, do zapłaty (kwota zł), podpis pracownika +
wyliczanie nominałów +
dodawanie (przedział czasowy -> lista pracowników objętych wypłatą, czyli tych, którzy zebrali w wybranym przedziale czasu >0 koszyków, premiowanie) +
wprowadzanie stawki na sezon w aplikacji (moduł wypłaty) +
premia ujemna +
kolumna suma w zbiorach +
edycja a zamykanie okna +
busy indicator klepsydra +

drukowanie umowy
brakujące kody -> wyświetlanie imienia przypisanego pracownika na liście zagregowanych koszyków


UWAGI:
Aplikacja nie jest przystosowana do pracy na wielu komputerach.
