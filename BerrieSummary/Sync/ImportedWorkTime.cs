﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BerrieSummary.Sync
{
    public class ImportedWorkTime : INotifyPropertyChanged
    {
        private bool m_isImported;
        public bool IsImported
        {
            get
            {
                return m_isImported;
            }

            set
            {
                if (m_isImported != value)
                {
                    m_isImported = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int CodeId { get; set; }
        public string Code { get; set; }
        public DateTime From { get; set; }
        public int PlantationId { get; set; }
        public DateTime? To { get; set; }
        public int TotalMinutes { get; set; }
        public string TotalTimeString
        {
            get
            {
                return TotalMinutes.FromMinutesToTimeString();
            }
        }

        private int ParseTimeString(string timeString)
        {
            int minutes = 0;
            if (timeString.Contains(':'))
            {
                var splitTimeString = timeString.Split(':');
                if(splitTimeString.Length > 0)
                {
                    int parsedHours;
                    if(int.TryParse(splitTimeString[0].Trim(), out parsedHours))
                    {
                        minutes += parsedHours * 60;
                    }
                }

                if(splitTimeString.Length > 1)
                {
                    int parsedMinutes;
                    if (int.TryParse(splitTimeString[1].Trim(), out parsedMinutes))
                    {
                        minutes += parsedMinutes;
                    }
                }
            }

            return minutes;
        }

        public string WorkerFullName { get; set; }
        public string PlantationName
        {
            get
            {
                return new PlantationsCache()[PlantationId].Name;
            }
        }

        public string DateString
        {
            get
            {
                return From.ToString("dd-MM-yyyy");
            }
        }

        public string TimeFrom
        {
            get
            {
                return From.ToShortTimeString();
            }

            set
            {
                From = CreateDateTime(From, value);
                NotifyPropertyChanged();
            }
        }

        public string TimeTo
        {
            get
            {
                return To.HasValue ? To.Value.ToShortTimeString() : null;
            }

            set
            {
                DateTime date;
                if (To.HasValue)
                {
                    date = To.Value;
                }
                else
                {
                    date = From;
                }

                if (string.IsNullOrEmpty(value))
                {
                    To = null;
                }
                else
                {
                    To = CreateDateTime(date, value);
                }

                NotifyPropertyChanged();
            }
        }

        public TimeSpan WorkingTimeSpan
        {
            get
            {
                if (To.HasValue && To >= From) return To.Value - From;
                return TimeSpan.Zero;
            }
        }

        private DateTime CreateDateTime(DateTime date, string time)
        {
            var splitTime = time.Split(':');
            int hour = int.Parse(splitTime[0]);
            int minute = splitTime.Length == 2 ? int.Parse(splitTime[1]) : 0;
            return new DateTime(date.Year, date.Month, date.Day, hour, minute, 0);
        }

        public ImportedWorkTime()
        {
            IsImported = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged()
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(null));
            }
        }
    }
}
