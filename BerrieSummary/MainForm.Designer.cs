﻿namespace BerrieSummary
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.plantationsDDL = new System.Windows.Forms.ToolStripComboBox();
            this.SyncStatusLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ustawieniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HarvestsFormButton = new System.Windows.Forms.Button();
            this.PaymentsFormButton = new System.Windows.Forms.Button();
            this.WorkersFormButton = new System.Windows.Forms.Button();
            this.SyncFormButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.HarvestsFormButton);
            this.groupBox1.Controls.Add(this.PaymentsFormButton);
            this.groupBox1.Controls.Add(this.WorkersFormButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(647, 210);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zarządzanie plantacją";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.plantationsDDL,
            this.ustawieniaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(899, 27);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.toolStripTextBox1.Enabled = false;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.ReadOnly = true;
            this.toolStripTextBox1.Size = new System.Drawing.Size(110, 23);
            this.toolStripTextBox1.Text = "Wybrana plantacja: ";
            // 
            // plantationsDDL
            // 
            this.plantationsDDL.BackColor = System.Drawing.Color.MistyRose;
            this.plantationsDDL.ForeColor = System.Drawing.Color.OrangeRed;
            this.plantationsDDL.Name = "plantationsDDL";
            this.plantationsDDL.Size = new System.Drawing.Size(121, 23);
            // 
            // SyncStatusLabel
            // 
            this.SyncStatusLabel.AutoSize = true;
            this.SyncStatusLabel.ForeColor = System.Drawing.Color.Chocolate;
            this.SyncStatusLabel.Location = new System.Drawing.Point(702, 69);
            this.SyncStatusLabel.Name = "SyncStatusLabel";
            this.SyncStatusLabel.Size = new System.Drawing.Size(160, 13);
            this.SyncStatusLabel.TabIndex = 10;
            this.SyncStatusLabel.Text = "Pliki oczekują na synchronizację";
            this.SyncStatusLabel.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "RESET";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // ustawieniaToolStripMenuItem
            // 
            this.ustawieniaToolStripMenuItem.Name = "ustawieniaToolStripMenuItem";
            this.ustawieniaToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ustawieniaToolStripMenuItem.Size = new System.Drawing.Size(162, 23);
            this.ustawieniaToolStripMenuItem.Text = "Ustawienia kopii zapasowej";
            this.ustawieniaToolStripMenuItem.Click += new System.EventHandler(this.ustawieniaToolStripMenuItem_Click);
            // 
            // HarvestsFormButton
            // 
            this.HarvestsFormButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.HarvestsFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HarvestsFormButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HarvestsFormButton.Image = global::BerrieSummary.Properties.Resources.basket;
            this.HarvestsFormButton.Location = new System.Drawing.Point(242, 45);
            this.HarvestsFormButton.Name = "HarvestsFormButton";
            this.HarvestsFormButton.Size = new System.Drawing.Size(161, 135);
            this.HarvestsFormButton.TabIndex = 10;
            this.HarvestsFormButton.Text = "Zbiory";
            this.HarvestsFormButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.HarvestsFormButton.UseVisualStyleBackColor = false;
            this.HarvestsFormButton.Click += new System.EventHandler(this.HarvestsFormButton_Click);
            // 
            // PaymentsFormButton
            // 
            this.PaymentsFormButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.PaymentsFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PaymentsFormButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PaymentsFormButton.Image = global::BerrieSummary.Properties.Resources.money;
            this.PaymentsFormButton.Location = new System.Drawing.Point(439, 45);
            this.PaymentsFormButton.Name = "PaymentsFormButton";
            this.PaymentsFormButton.Size = new System.Drawing.Size(161, 135);
            this.PaymentsFormButton.TabIndex = 9;
            this.PaymentsFormButton.Text = "Wypłaty";
            this.PaymentsFormButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PaymentsFormButton.UseVisualStyleBackColor = false;
            this.PaymentsFormButton.Click += new System.EventHandler(this.PaymentsFormButton_Click);
            // 
            // WorkersFormButton
            // 
            this.WorkersFormButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.WorkersFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WorkersFormButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.WorkersFormButton.ForeColor = System.Drawing.Color.Black;
            this.WorkersFormButton.Image = global::BerrieSummary.Properties.Resources.workers;
            this.WorkersFormButton.Location = new System.Drawing.Point(49, 45);
            this.WorkersFormButton.Name = "WorkersFormButton";
            this.WorkersFormButton.Size = new System.Drawing.Size(161, 135);
            this.WorkersFormButton.TabIndex = 8;
            this.WorkersFormButton.Text = "Pracownicy";
            this.WorkersFormButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.WorkersFormButton.UseVisualStyleBackColor = false;
            this.WorkersFormButton.Click += new System.EventHandler(this.WorkersFormButton_Click);
            // 
            // SyncFormButton
            // 
            this.SyncFormButton.BackColor = System.Drawing.Color.Orange;
            this.SyncFormButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SyncFormButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SyncFormButton.Image = global::BerrieSummary.Properties.Resources.sync;
            this.SyncFormButton.Location = new System.Drawing.Point(702, 95);
            this.SyncFormButton.Name = "SyncFormButton";
            this.SyncFormButton.Size = new System.Drawing.Size(161, 135);
            this.SyncFormButton.TabIndex = 3;
            this.SyncFormButton.Text = "Synchronizuj";
            this.SyncFormButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.SyncFormButton.UseVisualStyleBackColor = false;
            this.SyncFormButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 289);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SyncStatusLabel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.SyncFormButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BerrieSummary";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button SyncFormButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button HarvestsFormButton;
        private System.Windows.Forms.Button PaymentsFormButton;
        private System.Windows.Forms.Button WorkersFormButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripComboBox plantationsDDL;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.Label SyncStatusLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem ustawieniaToolStripMenuItem;
    }
}

