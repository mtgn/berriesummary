﻿using System;

namespace BerrieSummary
{
    public class BackupProgressEventArgs : EventArgs
    {
        public int Percent { get; private set; }

        public BackupProgressEventArgs(int percent)
        {
            Percent = percent;
        }
    }
}