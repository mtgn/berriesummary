﻿using System;

namespace BerrieSummary
{
    public class CodesHelper
    {
        public const string WORKER_PREFIX = "0";
        public const string BASKET_PREFIX = "1";
        public static string GetWorkerCode(string code)
        {
            if(code.StartsWith(WORKER_PREFIX))
            {
                return code;
            }
            else
            {
                LogConversion(WORKER_PREFIX);
                return string.Format("{0}{1}", WORKER_PREFIX, code.Substring(1));
            }
        }

        private static void LogConversion(string destinationPrefix)
        {
            //Console.WriteLine("Code conversion! (->{0})", destinationPrefix);
        }

        public static string GetBasketCode(string code)
        {
            if(code.StartsWith(BASKET_PREFIX))
            {
                return code;
            }
            else
            {
                LogConversion(BASKET_PREFIX);
                return string.Format("{0}{1}", BASKET_PREFIX, code.Substring(1));
            }
        }

        public static bool IsWorkerCode(string code)
        {
            return !string.IsNullOrEmpty(code) && code.StartsWith(WORKER_PREFIX);
        }
    }
}