﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BerrieSummary.Sync;

namespace BerrieSummary.Workers
{
    public partial class WorkerDetailsForm : Form
    {
        private int m_plantationId = -1;
        private int m_workerId = -1;
        private ViewMode m_viewMode = ViewMode.Add;

        public WorkerDetailsForm(int plantationId)
        {
            InitializeComponent();
            m_plantationId = plantationId;
        }

        private void SetControlsAccessibility()
        {
            if(m_viewMode == ViewMode.Preview)
            {
                CodeTextBox.ReadOnly = true;
                FirstNameTextBox.ReadOnly = true;
                LastNameTextBox.ReadOnly = true;
                AddressTextBox.ReadOnly = true;
                NipTextBox.ReadOnly = true;
                PeselTextBox.ReadOnly = true;
                RevenueOfficeTextBox.ReadOnly = true;
                InsuranceDDL.Enabled = false;

                SaveButton.Visible = false;
            }
            else if(m_viewMode == ViewMode.Edit)
            {
                CodeTextBox.ReadOnly = true;
            }
        }

        public WorkerDetailsForm(int plantationId, int selectedWorkerId, bool isReadonly)
            :this(plantationId)
        {
            m_workerId = selectedWorkerId;
            m_viewMode = isReadonly ? ViewMode.Preview : ViewMode.Edit;
            FillForm();

            SetControlsAccessibility();
        }

        public WorkerDetailsForm(int plantationId, string missingCode) 
            : this(plantationId)
        {
            if(!string.IsNullOrEmpty(missingCode))
            {
                CodeTextBox.Text = missingCode;
                CodeTextBox.ReadOnly = true;
            }
        }

        private void FillForm()
        {
            var worker = DatabaseHelper.GetWorkerById(m_workerId);
            CodeTextBox.Text = DatabaseHelper.GetWorkerCode(m_plantationId, m_workerId).Code;
            FirstNameTextBox.Text = worker.FirstName;
            LastNameTextBox.Text = worker.LastName;
            AddressTextBox.Text = worker.Address;
            NipTextBox.Text = worker.Nip;
            PeselTextBox.Text = worker.Pesel;
            RevenueOfficeTextBox.Text = worker.RevenueOffice;
            InsuranceDDL.SelectedItem = worker.InsuranceInfo;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            if (m_viewMode == ViewMode.Add)
            {
                var worker = GetWorker();
                var workerCode = GetWorkerCode();
                if (DatabaseHelper.IsCodeAvailable(workerCode.Code, m_plantationId))
                {
                    if (DatabaseHelper.InsertWorker(worker, workerCode))
                    {
                        DialogResult = DialogResult.OK;
                    }
                }
                else
                {
                    MessageBox.Show("Ten kod jest już zajęty.");
                }
            }
            else if (m_viewMode == ViewMode.Edit)
            {
                var worker = GetWorker();
                if(DatabaseHelper.UpdateWorker(worker))
                {
                    DialogResult = DialogResult.OK;
                }
            }

            Close();
        }

        private WorkerCode GetWorkerCode()
        {
            return new WorkerCode()
            {
                Code = CodesHelper.GetWorkerCode(CodeTextBox.Text),
                PlantationId = m_plantationId
            };
        }

        private Worker GetWorker()
        {
            return new Worker()
            {
                WorkerId = m_workerId,
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                Address = AddressTextBox.Text,
                Nip = NipTextBox.Text,
                Pesel = PeselTextBox.Text,
                RevenueOffice = RevenueOfficeTextBox.Text,
                InsuranceInfo = InsuranceDDL.SelectedItem != null ? InsuranceDDL.SelectedItem.ToString() : null
            };
        }
    }
}
