﻿using BerrieSummary.Sync;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace BerrieSummary
{
    public partial class SyncForm : Form
    {
        private DataSyncHelper m_dataSyncHelper = new DataSyncHelper();
        private Dictionary<string, int> SavedRows = new Dictionary<string, int>();

        public SyncForm()
        {
            InitializeComponent();

            m_dataSyncHelper.OnDataChanged += m_dataSyncHelper_OnDataChanged;
            m_dataSyncHelper.ParseFiles();

            if(m_dataSyncHelper.DuplicatedCodes.Any())
            {
                ShowDuplicatedCodesForm();
            }

            SetGrids();
        }

        private void ShowDuplicatedCodesForm()
        {
            using (var duplicatedCodesForm = new CodesReplaceForm(m_dataSyncHelper, CodeReplacementCause.Duplicated))
            {
                if (duplicatedCodesForm.ShowDialog() == DialogResult.OK)
                {
                    m_dataSyncHelper.UpdateDuplicatedCodes(duplicatedCodesForm.NewCodes);
                }
                else
                {
                    Close();
                }
            }
        }

        private void SetGrids()
        {
            NewWorkersGrid.AutoGenerateColumns = false;
            ImportedBasketsGrid.AutoGenerateColumns = false;
            AggregatedBasketsGrid.AutoGenerateColumns = false;
            WorkTimeGrid.AutoGenerateColumns = false;

            NewWorkersGrid.DataSource = m_dataSyncHelper.ImportedWorkers;
            WorkTimeGrid.DataSource = m_dataSyncHelper.ImportedWorkTime;
            ImportedBasketsGrid.DataSource = m_dataSyncHelper.ImportedBaskets;
            AggregatedBasketsGrid.DataSource = m_dataSyncHelper.AggregatedBaskets;
        }

        private void m_dataSyncHelper_OnDataChanged(object sender, EventArgs e)
        {
            if (wizardControl1.SelectedIndex == 1)
            {
                RefreshGridData(AggregatedBasketsGrid);
            }
        }

        private void RefreshGridData(DataGridView grid)
        {
            grid.AutoGenerateColumns = false;
            if (grid.Name == WorkTimeGrid.Name)
            {
                grid.DataSource = null;
                grid.DataSource = m_dataSyncHelper.ImportedWorkTime;
            }

            if (grid.Name == ImportedBasketsGrid.Name)
            {
                grid.DataSource = null;
                grid.DataSource = m_dataSyncHelper.ImportedBaskets;
            }

            if (grid.Name == AggregatedBasketsGrid.Name)
            {
                grid.DataSource = null;
                grid.DataSource = m_dataSyncHelper.AggregatedBaskets;
            }
        }

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            if (wizardControl1.SelectedIndex == 0)
            {
                if (m_dataSyncHelper.MissingCodes.Any())
                {
                    ShowMissingCodesForm();
                }
            }
            else if (wizardControl1.SelectedIndex == 1)
            {
                Cursor = Cursors.WaitCursor;
                var isSaved = m_dataSyncHelper.SaveToDB();
                if(!isSaved)
                {
                    if(MessageBox.Show("W czasie importu wystąpił błąd. Spróbuj ponownie.", 
                        "Błąd importu", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                    {
                        Close();
                    }
                }
            }

            if (wizardControl1.SelectedIndex + 1 < wizardControl1.TabCount)
            {
                wizardControl1.SelectTab(wizardControl1.SelectedIndex + 1);
            }
            else
            {
                Cursor = Cursors.Default;
                decimal basketsCount = m_dataSyncHelper.ImportedBaskets.Where(b => b.IsImported).Sum(b=>b.FillingLevel);
                int workersCount = m_dataSyncHelper.ImportedWorkers.Count();
                int workTimeCount = m_dataSyncHelper.ImportedWorkTime.Where(wt => wt.IsImported).Sum(wt => wt.TotalMinutes);
                if (basketsCount + workTimeCount + workersCount > 0)
                {
                    if (ShowImportSummaryMessageBox(basketsCount, workersCount, workTimeCount) == DialogResult.OK)
                    {
                        Close();
                    }
                }
                else
                {
                    Close();
                }
            }
        }

        private void ShowMissingCodesForm()
        {
            using (var missingCodesForm = new CodesReplaceForm(m_dataSyncHelper, CodeReplacementCause.Missing))
            {
                if (missingCodesForm.ShowDialog() == DialogResult.OK)
                {
                    m_dataSyncHelper.UpdateMissingCodes(missingCodesForm.NewCodes);
                    RefreshGridData(AggregatedBasketsGrid);
                }
                else
                {
                    Close();
                }
            }
        }

        private static DialogResult ShowImportSummaryMessageBox(decimal basketsCount, int workersCount, int workTimeCount)
        {
            return MessageBox.Show(string.Format("Import zakończony powodzeniem.\nZaimportowano:\n- koszyków: {0:0.0},\n- nowych pracowników: {1},\n- godzin pracy: {2}",
                                                    basketsCount,
                                                    workersCount,
                                                    workTimeCount.FromMinutesToTimeString()),
                                    "Podsumowanie importu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void SetControlsAccessibility()
        {
            switch(wizardControl1.SelectedIndex)
            {
                case 0: NextStepButton.Text = "Dalej"; break;
                case 1: NextStepButton.Text = "Zakończ import"; break;
            }
        }

        private void wizardControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlsAccessibility();
        }

        private void ImportedBasketsGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            RefreshGridData(AggregatedBasketsGrid);
        }

        private void ImportedBasketsGrid_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            // End of edition on each click on column of checkbox
            if (e.ColumnIndex == IsImported.Index && e.RowIndex != -1)
            {
                ImportedBasketsGrid.EndEdit();
            }
        }

        private bool IsTheSameCellValue(int column, int row)
        {
            DataGridViewCell cell1 = WorkTimeGrid[column, row];
            DataGridViewCell cell2 = WorkTimeGrid[column, row - 1];
            if (cell1.Value == null || cell2.Value == null)
            {
                return false;
            }

            return cell1.Value.ToString() == cell2.Value.ToString();
        }

        private void WorkTimeGrid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
            if (e.RowIndex < 1 || e.ColumnIndex < 0)
            {
                return;
            }

            if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex) && e.ColumnIndex != wtIsImported.Index && e.ColumnIndex != wtHours.Index)
            {
                e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
            }
            else
            {
                e.AdvancedBorderStyle.Top = WorkTimeGrid.AdvancedCellBorderStyle.Top;
            }
        }

        private void WorkTimeGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                return;
            }

            if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
            {
                if (e.DesiredType == typeof(string) 
                    && e.ColumnIndex != wtFrom.Index 
                    && e.ColumnIndex != wtTo.Index
                    && e.ColumnIndex != wtHours.Index)
                {
                    e.Value = string.Empty;
                }

                e.FormattingApplied = true;
            }
        }

        private void WorkTimeGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == wtIsImported.Index)
            {
                LoadScrollPosition(WorkTimeGrid);
            }
        }

        private void WorkTimeGrid_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            // End of edition on each click on column of checkbox
            if (e.ColumnIndex == wtIsImported.Index && e.RowIndex != -1)
            {
                SaveScrollPosition(WorkTimeGrid);
                WorkTimeGrid.EndEdit();
            }
        }

        private void SaveScrollPosition(DataGridView grid)
        {
            EnsureGridScrollConfig(grid);
            int rowToSave = 0;
            if (grid.Rows.Count > 0)
            {
                rowToSave = grid.FirstDisplayedCell.RowIndex;
                SavedRows[grid.Name] = rowToSave;
            }
        }

        private void EnsureGridScrollConfig(DataGridView grid)
        {
            if(!SavedRows.ContainsKey(grid.Name))
            {
                SavedRows.Add(grid.Name, 0);
            }
        }

        private void LoadScrollPosition(DataGridView grid)
        {
            EnsureGridScrollConfig(grid);
            int savedRow = SavedRows[grid.Name];
            if (savedRow != 0 && savedRow < grid.Rows.Count)
            {
                grid.FirstDisplayedScrollingRowIndex = savedRow;
            }
        }

        private void ImportedBasketsGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void ImportedBasketsGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != FillingLevel.Index)
            {
                return;
            }

            var value = e.FormattedValue.ToString();

            if (!string.IsNullOrEmpty(value))
            {
                var acceptedFillingLevels = new List<decimal>
                {
                    0.1M, 0.2M, 0.3M, 0.4M, 0.5M, 0.6M, 0.7M, 0.8M, 0.9M, 1.0M
                };

                decimal result = 0;
                if (!(decimal.TryParse(value, out result) && acceptedFillingLevels.Contains(result)))
                {
                    MessageBox.Show("Procent napełnienia koszyka może przyjmować jedynie wartości:\n0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0");
                    e.Cancel = true;
                }
            }
        }

        private void WorkTimeGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != wtFrom.Index && e.ColumnIndex != wtTo.Index)
            {
                return;
            }

            var value = e.FormattedValue.ToString();
            if (!string.IsNullOrEmpty(value))
            {
                string pattern = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
                if (!Regex.IsMatch(value, pattern))
                {
                    MessageBox.Show("Nie udało się odczytać wartości godziny.\nPodaj godzinę w formacie HH:mm.");
                    e.Cancel = true;
                }
            }
            else
            {
                if (e.ColumnIndex == wtFrom.Index)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
