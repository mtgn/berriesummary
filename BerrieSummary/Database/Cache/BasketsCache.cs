﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BerrieSummary
{
    public class BasketsCache : IEnumerable<BasketAggregate>
    {
        private BindingList<BasketAggregate> m_basketAggregates = new BindingList<BasketAggregate>();

        public BasketsCache(int plantationId)
        {
            m_basketAggregates = DatabaseHelper.GetBaskets(plantationId);
        }

        public BasketsCache(int plantationId, int codeId)
        {
            m_basketAggregates = DatabaseHelper.GetBaskets(plantationId, codeId);
        }

        public BasketAggregate this[int id]
        {
            get
            {
                return m_basketAggregates.FirstOrDefault(b => b.BasketId == id);
            }
        }

        public IEnumerable<BasketAggregate> this[DateTime date]
        {
            get
            {
                return m_basketAggregates.Where(b => b.CollectionDate == date);
            }
        }

        public IEnumerator<BasketAggregate> GetEnumerator()
        {
            return m_basketAggregates.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
