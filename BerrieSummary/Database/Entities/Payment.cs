﻿using System;

namespace BerrieSummary
{
    public class Payment
    {
        public int PaymentId { get; set; }
        public int PlantationId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}