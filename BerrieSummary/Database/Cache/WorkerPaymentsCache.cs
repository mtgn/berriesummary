﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary
{
    public class WorkerPaymentsCache : IEnumerable<WorkerPayment>
    {
        private BindingList<WorkerPayment> m_workerPayments = new BindingList<WorkerPayment>();

        public WorkerPaymentsCache(int paymentId)
        {
            m_workerPayments = DatabaseHelper.GetWorkerPayments(paymentId);
        }

        public WorkerPayment this[int workerId]
        {
            get
            {
                return m_workerPayments.FirstOrDefault(p => p.WorkerId == workerId);
            }
        }

        public IEnumerator<WorkerPayment> GetEnumerator()
        {
            return m_workerPayments.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}