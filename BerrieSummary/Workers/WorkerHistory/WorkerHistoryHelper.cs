﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary.Workers
{
    public class WorkerHistoryHelper
    {
        private int m_plantationId = -1;
        private int m_workerCodeId = -1;
        private int m_workerId = -1;

        private BasketsCache m_basketsCache;
        private WorkTimeCache m_workTimeCache;
        private BasketRatesCache m_basketRatesCache;

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Code { get; private set; }

        public SortableBindingList<WorkerDailySummary> History { get; private set; }

        public WorkerHistoryHelper(int plantationId, int codeId)
        {
            m_plantationId = plantationId;
            m_workerCodeId = codeId;
            m_workerId = DatabaseHelper.GetWorkerByCodeId(codeId).WorkerId;

            var worker = DatabaseHelper.GetWorkerByCodeId(codeId);
            FirstName = worker.FirstName;
            LastName = worker.LastName;
            Code = DatabaseHelper.GetCodeById(codeId);

            History = new SortableBindingList<WorkerDailySummary>();

            FillWorkerHistoryList();
        }

        public Dictionary<DateTime, string> GetRecentHistory(int daysCount)
        {
            var recentHistory = new Dictionary<DateTime, string>();
            var recentHistoryFull = History.OrderByDescending(h=>h.Date).Where(h=>h.Efficiency.HasValue).Take(5);
            foreach (var h in recentHistoryFull)
            {
                recentHistory.Add(h.Date, string.Format("{0:0.0} ({1:0.0})", h.BasketsQuantity, h.Efficiency));
            }

            return recentHistory;
        }

        private void FillWorkerHistoryList()
        {
            m_basketsCache = new BasketsCache(m_plantationId, m_workerCodeId);
            m_workTimeCache = new WorkTimeCache(m_plantationId, m_workerCodeId);
            m_basketRatesCache = new BasketRatesCache(m_plantationId);

            var datesWithBaskets = m_basketsCache.Where(b => b.Quantity > 0).Select(b => b.CollectionDate);
            var datesWithWorkTime = m_workTimeCache.Where(t => t.Minutes > 0).Select(t => t.Date);
            var dates = datesWithBaskets.Union(datesWithWorkTime).ToList();

            foreach(var date in dates)
            {
                var basketsThisDay = m_basketsCache[date];
                var workTimeThisDay = m_workTimeCache[date];
                var basketRate = m_basketRatesCache[date];
                var historySummary = new WorkerDailySummary()
                {
                    PlantationId = m_plantationId,
                    Date = date,
                    BasketsQuantity = basketsThisDay.Sum(b => b.Quantity),
                    WorkingMinutes = workTimeThisDay.Sum(t => t.Minutes),
                    BasketRate = basketRate == null ? (decimal?)null : basketRate.Rate
                };

                if (historySummary.BasketsQuantity > 0 || historySummary.WorkingMinutes > 0)
                {
                    History.Add(historySummary);
                }
            }
        }

        public void RefreshCache()
        {
            History = new SortableBindingList<WorkerDailySummary>();
            FillWorkerHistoryList();
        }
    }
}