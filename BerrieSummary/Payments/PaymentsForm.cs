﻿using BerrieSummary.Harvests;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary.Payments
{
    public partial class PaymentsForm : Form
    {
        private int m_plantationId = -1;
        private PaymentsHelper m_paymentsHelper;

        private int? SelectedPaymentId
        {
            get
            {
                if (PaymentsGrid.SelectedRows.Count == 0)
                {
                    return null;
                }

                var row = PaymentsGrid.SelectedRows[0];
                return (int)row.Cells[pPaymentId.Index].Value;
            }
        }

        public PaymentsForm()
        {
            InitializeComponent();
        }

        public PaymentsForm(int plantationId)
            : this()
        {
            m_plantationId = plantationId;
            m_paymentsHelper = new PaymentsHelper(plantationId);
            RefreshGrid();
        }

        private void AddPayrollButton_Click(object sender, EventArgs e)
        {
            if(!ValidateDates())
            {
                return;
            }

            bool showPayrolls = false;
            int? newPaymentId = null;
            using (var form = new PickDatesForm(m_plantationId))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    var payment = form.GetPayment();
                    newPaymentId = DatabaseHelper.InsertPayment(payment);
                    if (newPaymentId.HasValue)
                    {
                        showPayrolls = true;
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się utworzyć listy płac. Spróbuj ponownie.");
                    }
                }
            }

            if (showPayrolls)
            {
                ShowPayroll(newPaymentId.Value);
            }
        }

        private bool ValidateDates()
        {
            var maxDate = DatabaseHelper.GetMaxDate(m_plantationId);
            var suggestedDateFrom = DatabaseHelper.GetSuggestedNextPaymentDateFrom(m_plantationId);

            if (maxDate == null)
            {
                MessageBox.Show("Brak koszyków do wykonania wypłaty.");
                return false;
            }

            if (suggestedDateFrom == null)
            {
                MessageBox.Show("Wszystkie zarejestrowane koszyki są już objęte wypłatami.");
                return false;
            }

            return true;
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            m_paymentsHelper.RefreshCache();
            PaymentsGrid.AutoGenerateColumns = false;
            PaymentsGrid.DataSource = null;
            PaymentsGrid.DataSource = m_paymentsHelper.PaymentSummary;
        }

        private void EditPayrollButton_Click(object sender, EventArgs e)
        {
            if (SelectedPaymentId.HasValue)
            {
                bool showPayrolls = false;
                using (var form = new PickDatesForm(m_plantationId, SelectedPaymentId))
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        var payment = form.GetPayment();
                        DatabaseHelper.UpdatePayment(payment);
                        showPayrolls = true;
                    }
                }

                if (showPayrolls)
                {
                    ShowPayroll(SelectedPaymentId.Value);
                }
            }
        }

        private void DeletePayrollButton_Click(object sender, EventArgs e)
        {
            if (SelectedPaymentId.HasValue)
            {
                if (MessageBox.Show("Czy na pewno chcesz usunąć wybraną wypłatę?",
                    "Usuwanie wypłaty", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (DatabaseHelper.DeletePayment(SelectedPaymentId.Value))
                    {
                        RefreshGrid();
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się usunąć wypłaty.", "Usuwanie wypłaty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void PreviewPayrollButton_Click(object sender, EventArgs e)
        {
            if (SelectedPaymentId.HasValue)
            {
                ShowPayroll(SelectedPaymentId.Value);
            }
        }

        private void ShowPayroll(int paymentId)
        {
            var form = new PayrollForm(paymentId);
            form.FormClosed += Form_FormClosed;
            form.Show();
        }

        private void ChangeBaseRateButton_Click(object sender, EventArgs e)
        {
            using (var form = new ChangeBaseRateForm(m_plantationId))
            {
                form.ShowDialog();
            }
        }
    }
}
