﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BerrieSummary.Workers
{
    public partial class ChangeBasketsForm : Form
    {
        private int m_plantationId;
        private int m_codeId;

        private WorkerHistoryHelper m_historyHelper;

        private decimal? TotalBaskets
        {
            get
            {
                decimal quantity;
                if (decimal.TryParse(QuantityTextBox.Text, out quantity))
                {
                    return SignDDL.SelectedItem.ToString() == "-"
                        ? -1 * quantity
                        : quantity;
                }
                else
                {
                    return null;
                }
            }
        }

        private ChangeBasketsForm()
        {
            InitializeComponent();
        }

        public ChangeBasketsForm(int plantationId, int codeId, DateTime? date)
            :this()
        {
            m_plantationId = plantationId;
            m_codeId = codeId;
            m_historyHelper = new WorkerHistoryHelper(plantationId, codeId);

            SetDatePicker(date);
            SignDDL.SelectedIndex = 0;

            QuantityTextBox.Select();
        }

        private void SetDatePicker(DateTime? date)
        {
            DatePicker.MaxDate = DateTime.Today;
            DatePicker.Value = date.HasValue ? date.Value : DateTime.Today;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                var basket = GetBasketAggregate();
                if (basket.Quantity > 0 || ValidateQuantity(basket.CollectionDate, basket.Quantity))
                {
                    if (DatabaseHelper.InsertBasket(basket))
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        DialogResult = DialogResult.No;
                    }

                    Close();
                }
                else
                {
                    MessageBox.Show("Próbujesz odjąć zbyt dużo koszyków.");
                }
            }
            else
            {
                MessageBox.Show("Ilość koszyków musi być liczbą różną od zera.");
            }
        }

        private bool ValidateQuantity(DateTime date, decimal quantity)
        {
            var history = m_historyHelper.History.FirstOrDefault(h => h.Date == date);
            return history != null && history.BasketsQuantity + quantity >= 0;
        }

        private bool ValidateForm()
        {
            return TotalBaskets.HasValue && TotalBaskets.Value != 0;
        }

        private BasketAggregate GetBasketAggregate()
        {
            return new BasketAggregate()
            {
                CodeId = m_codeId,
                PlantationId = m_plantationId,
                CollectionDate = DatePicker.Value,
                Quantity = TotalBaskets.Value
            };
        }
    }
}
