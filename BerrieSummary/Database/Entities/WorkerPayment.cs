﻿namespace BerrieSummary
{
    public class WorkerPayment
    {
        public int WorkerId { get; set; }
        public int PaymentId { get; set; }
        public decimal Premium { get; set; }

    }
}
