﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BerrieSummary
{
    public class BasketRatesCache : IEnumerable<BasketPayRate>
    {
        private BindingList<BasketPayRate> m_basketRates = new BindingList<BasketPayRate>();

        public BasketRatesCache(int plantationId)
        {
            m_basketRates = DatabaseHelper.GetBasketRates(plantationId);
        }

        public BasketPayRate this[DateTime date]
        {
            get
            {
                return m_basketRates.FirstOrDefault(r => r.Date == date);
            }
        }

        public IEnumerator<BasketPayRate> GetEnumerator()
        {
            return m_basketRates.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
