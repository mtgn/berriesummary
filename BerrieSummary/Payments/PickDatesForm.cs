﻿using System;
using System.Windows.Forms;

namespace BerrieSummary.Payments
{
    public partial class PickDatesForm : Form
    {
        private int m_plantationId;
        private int? m_paymentId;
        private PickDatesForm()
        {
            InitializeComponent();
        }

        public PickDatesForm(int plantationId, int? paymentId = null)
            :this()
        {
            m_plantationId = plantationId;
            m_paymentId = paymentId;
            SetDatePickers();
        }

        private void SetDatePickers()
        {
            var minDate = DatabaseHelper.GetMinDate(m_plantationId);
            var maxDate = DatabaseHelper.GetMaxDate(m_plantationId);
            var suggestedDateFrom = DatabaseHelper.GetSuggestedNextPaymentDateFrom(m_plantationId);

            FromDatePicker.MaxDate = maxDate.Value;
            FromDatePicker.MinDate = minDate.Value;
            ToDatePicker.MaxDate = maxDate.Value;
            ToDatePicker.MinDate = minDate.Value;

            if (m_paymentId.HasValue)
            {
                var payment = DatabaseHelper.GetPayment(m_paymentId.Value);
                FromDatePicker.Value = payment.From;
                ToDatePicker.Value = payment.To;
            }
            else
            {
                FromDatePicker.Value = suggestedDateFrom.Value;
                ToDatePicker.Value = maxDate.Value;
            }
        }

        public Payment GetPayment()
        {
            return new Payment()
            {
                PaymentId = m_paymentId ?? -1,
                PlantationId = m_plantationId,
                From = FromDatePicker.Value,
                To = ToDatePicker.Value,
            };
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (BasketRatesExist())
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                MessageBox.Show("Przed wykonaniem wypłaty uzupełnij dzienne stawki za koszyk w module Zbiory.");
            }
        }

        private bool BasketRatesExist()
        {
            return DatabaseHelper.BasketRatesExist(m_plantationId, FromDatePicker.Value, ToDatePicker.Value);
        }
    }
}
