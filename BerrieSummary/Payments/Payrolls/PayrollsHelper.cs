﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using BerrieSummary.Harvests;

namespace BerrieSummary.Payments
{
    public class PayrollsHelper
    {
        private WorkerPaymentsCache m_workerPaymentsCache;

        public Payment Payment { get; private set; }
        public SortableBindingList<Payroll> Payrolls { get; private set; }

        public PayrollsHelper(int paymentId)
        {
            Payment = DatabaseHelper.GetPayment(paymentId);
            CreatePayrollsList();
        }

        public void RefreshCache()
        {
            CreatePayrollsList();
        }

        private void CreatePayrollsList()
        {
            if(Payment == null)
            {
                return;
            }

            m_workerPaymentsCache = new WorkerPaymentsCache(Payment.PaymentId);
            var harvestHelper = new HarvestsHelper(Payment.PlantationId);
            var payrollsList = new List<Payroll>();
            var workPerCodeList = DatabaseHelper.GetWorkPerCode(Payment.PlantationId, Payment.From, Payment.To);

            foreach (var c in workPerCodeList.OrderBy(c=>c.Code))
            {
                var worker = DatabaseHelper.GetWorkerById(c.WorkerId);
                var workerPayment = m_workerPaymentsCache[c.WorkerId];

                payrollsList.Add(new Payroll()
                {
                    PaymentId = Payment.PaymentId,
                    WorkerId = c.WorkerId,
                    Code = c.Code,
                    FullName = worker.FullName,
                    BasketsQuantity = c.Baskets.Sum(b => b.Quantity),
                    WorkValue = c.Baskets.Sum(b => b.Quantity * harvestHelper.HarvestsSummary
                                                                .FirstOrDefault(h => h.DateOfHarvest == b.CollectionDate)
                                                                .BasketRate.Value),
                    Premium = workerPayment == null ? (decimal?)null : workerPayment.Premium
                });
            }

            Payrolls = new SortableBindingList<Payroll>(payrollsList);
        }
    }
}