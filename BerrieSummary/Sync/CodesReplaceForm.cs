﻿using BerrieSummary.Workers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary.Sync
{
    public partial class CodesReplaceForm : Form
    {
        private BindingList<WorkerCodePair> m_codePairs;
        private CodeReplacementCause m_cause;
        private List<string> m_unavailableCodes;

        private WorkerCodePair SelectedItem
        {
            get
            {
                if(dataGridView1.SelectedCells.Count == 0)
                {
                    return null;
                }

                var row = dataGridView1.SelectedCells[0].OwningRow;
                return row.DataBoundItem as WorkerCodePair;
            }
        }

        public IEnumerable<WorkerCodePair> NewCodes
        {
            get
            {
                return m_codePairs.Where(c => !string.IsNullOrEmpty(c.NewCode) && c.OldCode != c.NewCode);
            }
        }

        public CodesReplaceForm(DataSyncHelper syncHelper, CodeReplacementCause cause)
        {
            InitializeComponent();

            m_cause = cause;
            if (m_cause == CodeReplacementCause.Duplicated)
            {
                m_codePairs = syncHelper.DuplicatedCodes;
                m_unavailableCodes = syncHelper.ImportedWorkers.Select(w => w.Code).ToList();
            }
            else
            {
                m_codePairs = syncHelper.MissingCodes;
            }

            RefreshGrid();
            SetForm();
        }

        private void SetForm()
        {
            if(m_cause == CodeReplacementCause.Duplicated)
            {
                Text = "Zdublowane kody";
                PickWorkerButton.Visible = false;
                mcOldCode.HeaderText = "Zdublowany kod";
                dataGridView1.ReadOnly = false;
            }
        }

        private void RefreshGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = m_codePairs;
        }

        private void PickWorkerButton_Click(object sender, EventArgs e)
        {
            if (SelectedItem != null)
            {
                using (var form = new WorkersForm(SelectedItem.PlantationId, SelectedItem))
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        RefreshGrid();
                    }
                }
            }
        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if(m_codePairs.All(c=>!string.IsNullOrEmpty(c.NewCode)))
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                string messageText = m_cause == CodeReplacementCause.Missing
                    ? "Niektóre brakujące kody nadal nie mają przypisanego pracownika."
                    : "Niektóre zduplikowane kody nie zostały zaktualizowane.";
                MessageBox.Show(messageText, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (m_cause == CodeReplacementCause.Missing || e.ColumnIndex != mcNewCode.Index)
            {
                return;
            }

            var value = e.FormattedValue.ToString();
            if (!string.IsNullOrEmpty(value))
            {
                if (CodesHelper.IsWorkerCode(value))
                {
                    if (!DatabaseHelper.IsCodeAvailable(value, SelectedItem.PlantationId)
                        || m_unavailableCodes.Contains(value))
                    {
                        MessageBox.Show("Wpisany kod już jest wykorzystywany na tej plantacji.");
                        e.Cancel = true;
                    }
                }
                else
                {
                    MessageBox.Show("Kod pracownika powinien rozpoczynać się od prefiksu \"{0}\"", CodesHelper.WORKER_PREFIX);
                    e.Cancel = true;
                }
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
