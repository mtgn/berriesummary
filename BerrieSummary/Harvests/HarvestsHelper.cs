﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary.Harvests
{
    public class HarvestsHelper
    {
        private int m_plantationId;
        private WorkTimeCache m_workTimeCache;
        private BasketsCache m_basketsCache;
        private BasketRatesCache m_basketRatesCache;

        public SortableBindingList<HarvestDaySummary> HarvestsSummary { get; private set; }

        public HarvestsHelper(int plantationId)
        {
            m_plantationId = plantationId;
            HarvestsSummary = new SortableBindingList<HarvestDaySummary>();

            m_workTimeCache = new WorkTimeCache(m_plantationId);
            m_basketsCache = new BasketsCache(m_plantationId);
            m_basketRatesCache = new BasketRatesCache(m_plantationId);

            CreateSummary();
        }

        private void CreateSummary()
        {
            var listOfDays = m_basketsCache.Select(b => b.CollectionDate).Distinct();
            foreach(var day in listOfDays.OrderByDescending(d=>d))
            {
                var workTimeForDay = m_workTimeCache[day];
                var basketsForDay = m_basketsCache[day];
                var basketRateForDay = m_basketRatesCache[day];
                HarvestsSummary.Add(new HarvestDaySummary()
                {
                    PlantationId = m_plantationId,
                    DateOfHarvest = day,
                    WorkTimeMinutesCount = workTimeForDay.Sum(t => t.Minutes),
                    WorkersCount = basketsForDay.Select(b => b.CodeId).Distinct().Count(),
                    BasketsCount = basketsForDay.Sum(b => b.Quantity),
                    BasketRate = basketRateForDay == null ? null : basketRateForDay.Rate
                });
            }
        }

        public void SetBasketRate(DateTime date, decimal? basketRate)
        {
            if(basketRate.HasValue)
            {
                var basketPayRate = new BasketPayRate()
                {
                    PlantationId = m_plantationId,
                    Date = date,
                    Rate = basketRate.Value
                };

                DatabaseHelper.SetBasketRate(basketPayRate);
            }
        }

        public Dictionary<DateTime, double> CalculateRecentEfficiency(int codeId, int days)
        {
            var efficiencyDictionary = new Dictionary<DateTime, double>();
            var recentSummary = HarvestsSummary.OrderByDescending(hs => hs.DateOfHarvest).Take(days);
            foreach(var summary in recentSummary)
            {
                var basketsCount = m_basketsCache.Where(b => b.CodeId == codeId && b.CollectionDate == summary.DateOfHarvest).Sum(b => b.Quantity);
                var minutesCount = m_workTimeCache.Where(t => t.CodeId == codeId && t.Date == summary.DateOfHarvest).Sum(t => t.Minutes);
                var hoursCount = (double)minutesCount / 60;
                if (hoursCount > 0 && basketsCount > 0 && summary.AverageTempo.HasValue)
                {
                    var workerTempo = (double)basketsCount / hoursCount;
                    var workerEfficiency = workerTempo / summary.AverageTempo.Value;
                    efficiencyDictionary.Add(summary.DateOfHarvest, Math.Round(workerEfficiency, 2));
                }
            }

            return efficiencyDictionary;
        }
    }
}