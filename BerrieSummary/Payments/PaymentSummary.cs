﻿using System;

namespace BerrieSummary.Payments
{
    public class PaymentSummary
    {
        public int PaymentId { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public decimal BasketsQuantity { get; set; }
        public int SumOfWorkers { get; set; }
        public decimal TotalCost { get; set; }

        public string DateFromString
        {
            get
            {
                return DateFrom.ToString("dd-MM-yyyy");
            }
        }

        public string DateToString
        {
            get
            {
                return DateTo.ToString("dd-MM-yyyy");
            }
        }

        public string BasketsQuantityString
        {
            get
            {
                return BasketsQuantity.ToString("0.0");
            }
        }

        public string TotalCostString
        {
            get
            {
                return TotalCost.ToString("0 zł");
            }
        }
    }
}