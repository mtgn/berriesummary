﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary
{
    public class WorkersCache : IEnumerable<Worker>
    {
        private BindingList<Worker> m_workers = new BindingList<Worker>();
        private BindingList<WorkerCode> m_workerCodes = new BindingList<WorkerCode>();

        public WorkersCache(int plantationId)
        {
            m_workerCodes = DatabaseHelper.GetWorkerCodes(plantationId);
            m_workers = DatabaseHelper.GetWorkers(plantationId);
            FillCodes();
        }

        private void FillCodes()
        {
            foreach (var worker in m_workers)
            {
                var codeEntry = m_workerCodes.FirstOrDefault(c => c.WorkerId == worker.WorkerId);
                if (codeEntry != null)
                {
                    worker.Code = codeEntry.Code;
                }
            }
        }

        public WorkersCache()
        {
            m_workerCodes = DatabaseHelper.GetWorkerCodes();
            m_workers = DatabaseHelper.GetWorkers();
            FillCodes();
        }

        public Worker this[int id]
        {
            get
            {
                return m_workers.FirstOrDefault(p => p.WorkerId == id);
            }
        }

        public Worker this[string code]
        {
            get
            {
                var workerCode = m_workerCodes.FirstOrDefault(wc => wc.Code == CodesHelper.GetWorkerCode(code));
                if (workerCode != null)
                {
                    return m_workers.FirstOrDefault(w => w.WorkerId == workerCode.WorkerId);
                }

                return null;
            }
        }

        public Worker this[string code, int plantationId]
        {
            get
            {
                var workerCode = m_workerCodes.FirstOrDefault(wc => wc.Code == CodesHelper.GetWorkerCode(code) && wc.PlantationId == plantationId);
                if (workerCode != null)
                {
                    return m_workers.FirstOrDefault(w => w.WorkerId == workerCode.WorkerId);
                }

                return null;
            }
        }

        public IEnumerator<Worker> GetEnumerator()
        {
            return m_workers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}