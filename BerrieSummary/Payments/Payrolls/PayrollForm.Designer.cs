﻿namespace BerrieSummary.Payments
{
    partial class PayrollForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayrollForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PayrollsGrid = new System.Windows.Forms.DataGridView();
            this.wpCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wpFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wpBasketsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wpBasketRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wpWorkValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wpPremium = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wpTotalValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.DateToLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DateFromLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.NominalValuesButton = new System.Windows.Forms.Button();
            this.PrintButton = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.TotalAmountLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PayrollsGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.Controls.Add(this.PayrollsGrid, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(959, 580);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // PayrollsGrid
            // 
            this.PayrollsGrid.AllowUserToAddRows = false;
            this.PayrollsGrid.AllowUserToDeleteRows = false;
            this.PayrollsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PayrollsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wpCode,
            this.wpFullName,
            this.wpBasketsCount,
            this.wpBasketRate,
            this.wpWorkValue,
            this.wpPremium,
            this.wpTotalValue});
            this.PayrollsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PayrollsGrid.Location = new System.Drawing.Point(3, 3);
            this.PayrollsGrid.MultiSelect = false;
            this.PayrollsGrid.Name = "PayrollsGrid";
            this.PayrollsGrid.RowHeadersVisible = false;
            this.PayrollsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.PayrollsGrid.Size = new System.Drawing.Size(711, 574);
            this.PayrollsGrid.TabIndex = 1;
            this.PayrollsGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.PayrollsGrid_CellValidating);
            this.PayrollsGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.PayrollsGrid_CellValueChanged);
            this.PayrollsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.PayrollsGrid_DataError);
            // 
            // wpCode
            // 
            this.wpCode.DataPropertyName = "Code";
            this.wpCode.HeaderText = "Kod";
            this.wpCode.Name = "wpCode";
            this.wpCode.ReadOnly = true;
            this.wpCode.Width = 80;
            // 
            // wpFullName
            // 
            this.wpFullName.DataPropertyName = "FullName";
            this.wpFullName.HeaderText = "Pracownik";
            this.wpFullName.Name = "wpFullName";
            this.wpFullName.ReadOnly = true;
            this.wpFullName.Width = 180;
            // 
            // wpBasketsCount
            // 
            this.wpBasketsCount.DataPropertyName = "BasketsQuantityString";
            this.wpBasketsCount.HeaderText = "Zebranych koszyków";
            this.wpBasketsCount.Name = "wpBasketsCount";
            this.wpBasketsCount.ReadOnly = true;
            this.wpBasketsCount.Width = 83;
            // 
            // wpBasketRate
            // 
            this.wpBasketRate.DataPropertyName = "BasketRateString";
            this.wpBasketRate.HeaderText = "Średnia stawka za koszyk";
            this.wpBasketRate.Name = "wpBasketRate";
            this.wpBasketRate.ReadOnly = true;
            this.wpBasketRate.Width = 80;
            // 
            // wpWorkValue
            // 
            this.wpWorkValue.DataPropertyName = "WorkValueString";
            this.wpWorkValue.HeaderText = "Do zapłaty";
            this.wpWorkValue.Name = "wpWorkValue";
            this.wpWorkValue.ReadOnly = true;
            // 
            // wpPremium
            // 
            this.wpPremium.DataPropertyName = "Premium";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle1.Format = "0 zł";
            dataGridViewCellStyle1.NullValue = null;
            this.wpPremium.DefaultCellStyle = dataGridViewCellStyle1;
            this.wpPremium.HeaderText = "Premia";
            this.wpPremium.Name = "wpPremium";
            this.wpPremium.Width = 80;
            // 
            // wpTotalValue
            // 
            this.wpTotalValue.DataPropertyName = "TotalValueString";
            this.wpTotalValue.HeaderText = "RAZEM";
            this.wpTotalValue.Name = "wpTotalValue";
            this.wpTotalValue.ReadOnly = true;
            this.wpTotalValue.Width = 80;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.NominalValuesButton);
            this.panel1.Controls.Add(this.PrintButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(720, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 574);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Orange;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TotalAmountLabel);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DateToLabel);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DateFromLabel);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(10, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 157);
            this.panel2.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(32, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 21);
            this.label2.TabIndex = 18;
            this.label2.Text = "od";
            // 
            // DateToLabel
            // 
            this.DateToLabel.AutoSize = true;
            this.DateToLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DateToLabel.Location = new System.Drawing.Point(58, 68);
            this.DateToLabel.Margin = new System.Windows.Forms.Padding(10);
            this.DateToLabel.Name = "DateToLabel";
            this.DateToLabel.Size = new System.Drawing.Size(125, 30);
            this.DateToLabel.TabIndex = 17;
            this.DateToLabel.Text = "00-00-0000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(32, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 21);
            this.label1.TabIndex = 16;
            this.label1.Text = "od";
            // 
            // DateFromLabel
            // 
            this.DateFromLabel.AutoSize = true;
            this.DateFromLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DateFromLabel.Location = new System.Drawing.Point(58, 36);
            this.DateFromLabel.Margin = new System.Windows.Forms.Padding(10);
            this.DateFromLabel.Name = "DateFromLabel";
            this.DateFromLabel.Size = new System.Drawing.Size(125, 30);
            this.DateFromLabel.TabIndex = 15;
            this.DateFromLabel.Text = "00-00-0000";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(38, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 21);
            this.label4.TabIndex = 14;
            this.label4.Text = "Lista płac za okres";
            // 
            // NominalValuesButton
            // 
            this.NominalValuesButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.NominalValuesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NominalValuesButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NominalValuesButton.ForeColor = System.Drawing.Color.Black;
            this.NominalValuesButton.Image = global::BerrieSummary.Properties.Resources.money;
            this.NominalValuesButton.Location = new System.Drawing.Point(35, 383);
            this.NominalValuesButton.Name = "NominalValuesButton";
            this.NominalValuesButton.Size = new System.Drawing.Size(164, 125);
            this.NominalValuesButton.TabIndex = 11;
            this.NominalValuesButton.Text = "Oblicz nominały";
            this.NominalValuesButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.NominalValuesButton.UseVisualStyleBackColor = false;
            this.NominalValuesButton.Click += new System.EventHandler(this.NominalValuesButton_Click);
            // 
            // PrintButton
            // 
            this.PrintButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.PrintButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PrintButton.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PrintButton.ForeColor = System.Drawing.Color.Black;
            this.PrintButton.Image = global::BerrieSummary.Properties.Resources.save;
            this.PrintButton.Location = new System.Drawing.Point(35, 232);
            this.PrintButton.Name = "PrintButton";
            this.PrintButton.Size = new System.Drawing.Size(164, 121);
            this.PrintButton.TabIndex = 10;
            this.PrintButton.Text = "Zapisz wydruk";
            this.PrintButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PrintButton.UseVisualStyleBackColor = false;
            this.PrintButton.Click += new System.EventHandler(this.PrintButton_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "Plik XML|*.xml|Arkusz kalkulacyjny Microsoft Excel|*.xls";
            this.saveFileDialog1.Title = "Zapisz wydruk";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(24, 120);
            this.label3.Margin = new System.Windows.Forms.Padding(10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 21);
            this.label3.TabIndex = 20;
            this.label3.Text = "na kwotę";
            // 
            // TotalAmountLabel
            // 
            this.TotalAmountLabel.AutoSize = true;
            this.TotalAmountLabel.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TotalAmountLabel.Location = new System.Drawing.Point(98, 114);
            this.TotalAmountLabel.Margin = new System.Windows.Forms.Padding(10);
            this.TotalAmountLabel.Name = "TotalAmountLabel";
            this.TotalAmountLabel.Size = new System.Drawing.Size(95, 30);
            this.TotalAmountLabel.TabIndex = 19;
            this.TotalAmountLabel.Text = "00000 zł";
            // 
            // PayrollForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 580);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PayrollForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lista płac";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PayrollsGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView PayrollsGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label DateFromLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button NominalValuesButton;
        private System.Windows.Forms.Button PrintButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label DateToLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpBasketsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpBasketRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpWorkValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpPremium;
        private System.Windows.Forms.DataGridViewTextBoxColumn wpTotalValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label TotalAmountLabel;
    }
}