﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BerrieSummary.Harvests
{
    public class HarvestDaySummary
    {
        public int PlantationId { get; set; }
        public DateTime DateOfHarvest { get; set; }
        public int WorkersCount { get; set; }
        public int WorkTimeMinutesCount { get; set; }
        public decimal BasketsCount { get; set; }
        public decimal? BasketRate { get; set; }

        public string DateOfHarvestString
        {
            get
            {
                return DateOfHarvest.ToString("dd-MM-yyyy");
            }
        }
        public string WorkTimeCountString
        {
            get
            {
                return WorkTimeMinutesCount.FromMinutesToTimeString();
            }
        }
        public decimal DayCost
        {
            get
            {
                return ((decimal)WorkTimeMinutesCount / 60) * new PlantationsCache()[PlantationId].Base_Rate;
            }
        }
        public decimal? BasketCost
        {
            get
            {
                return BasketsCount == 0
                    ? (decimal?)null
                    : DayCost / BasketsCount;
            }
        }

        public double? AverageTempo
        {
            get
            {
                return WorkTimeMinutesCount == 0
                    ? (double?)null
                    : (double)Math.Round(BasketsCount / (WorkTimeMinutesCount / 60), 2);
            }
        }

        public decimal? DailyPayment
        {
            get
            {
                return BasketRate.HasValue
                    ? BasketRate * BasketsCount
                    : null;
            }
        }
    }
}
