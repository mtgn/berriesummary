﻿namespace BerrieSummary
{
    public enum ViewMode
    {
        Add,
        Edit,
        Preview
    }
}