﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BerrieSummary.Workers
{
    public partial class ChangeWorkTimeForm : Form
    {
        private int m_plantationId;
        private int m_codeId;

        private WorkerHistoryHelper m_historyHelper;

        private int? TotalMinutes
        {
            get
            {
                int hours, minutes;
                string hoursString = string.IsNullOrEmpty(HoursTextBox.Text) ? "0" : HoursTextBox.Text;
                string minutesString = string.IsNullOrEmpty(MinutesTextBox.Text) ? "0" : MinutesTextBox.Text;
                var isHourParsed = int.TryParse(hoursString, out hours);
                var isMinutesParsed = int.TryParse(minutesString, out minutes);

                if (isHourParsed && isMinutesParsed)
                {
                    var totalMinutes = hours * 60 + minutes;
                    return SignDDL.SelectedItem.ToString() == "-"
                        ? -1 * totalMinutes
                        : totalMinutes;
                }
                else
                {
                    return null;
                }
            }
        }

        private ChangeWorkTimeForm()
        {
            InitializeComponent();
        }

        private void SetDatePicker(DateTime? date)
        {
            DatePicker.MaxDate = DateTime.Today;
            DatePicker.Value = date.HasValue ? date.Value : DateTime.Today;
        }

        public ChangeWorkTimeForm(int plantationId, int codeId, DateTime? date)
            :this()
        {
            m_plantationId = plantationId;
            m_codeId = codeId;
            m_historyHelper = new WorkerHistoryHelper(plantationId, codeId);

            SetDatePicker(date);
            SignDDL.SelectedIndex = 0;

            HoursTextBox.Select();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                var workTime = GetWorkTime();
                if (ValidateMinutes(workTime.Date, workTime.Minutes))
                {
                    if (DatabaseHelper.InsertWorkTime(workTime))
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        DialogResult = DialogResult.No;
                    }

                    Close();
                }
                else
                {
                    MessageBox.Show("Próbujesz odjąć lub dodać zbyt dużo czasu pracy.");
                }
            }
            else
            {
                MessageBox.Show("Formularz zawiera nieprawidłowe wartości.");
            }
        }

        private bool ValidateMinutes(DateTime date, int minutes)
        {
            var history = m_historyHelper.History.FirstOrDefault(h => h.Date == date);
            return history == null && minutes <= 24 * 60
                || (history != null 
                    && history.WorkingMinutes + minutes >= 0 
                    && history.WorkingMinutes + minutes <= 24 * 60);
        }

        private bool ValidateForm()
        {
            return TotalMinutes.HasValue && TotalMinutes.Value != 0;
        }

        private WorkTime GetWorkTime()
        {
            return new WorkTime()
            {
                PlantationId = m_plantationId,
                Date = DatePicker.Value,
                Minutes = TotalMinutes.Value,
                CodeId = m_codeId
            };
        }
    }
}
