﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace BerrieSummary.Sync
{
    public class DataSyncHelper
    {
        private const string LOCAL_ARCHIVE_DIRECTORY_PATH = "archive\\";

        private WorkersCache m_workersCache = new WorkersCache();
        public delegate void DataChanged(object sender, EventArgs e);
        public event DataChanged OnDataChanged;

        private BindingList<BasketAggregate> m_aggregatedBaskets;
        private List<string> m_fileList = new List<string>();

        public BindingList<WorkerCodePair> MissingCodes { get; private set; }
        public BindingList<WorkerCodePair> DuplicatedCodes { get; private set; }
        public BindingList<ImportedBasket> ImportedBaskets { get; private set; }
        public BindingList<ImportedWorker> ImportedWorkers { get; private set; }
        public BindingList<ImportedWorkTime> ImportedWorkTime { get; private set; }
        public BindingList<BasketAggregate> AggregatedBaskets
        {
            get
            {
                return m_aggregatedBaskets;
            }
        }

        public DataSyncHelper()
        {
            ImportedBaskets = new BindingList<ImportedBasket>();
            ImportedWorkers = new BindingList<ImportedWorker>();
            ImportedWorkTime = new BindingList<ImportedWorkTime>();
            MissingCodes = new BindingList<WorkerCodePair>();
            DuplicatedCodes = new BindingList<WorkerCodePair>();

            ImportedBaskets.ListChanged += DataListChanged;
            ImportedWorkers.ListChanged += DataListChanged;
            ImportedWorkTime.ListChanged += DataListChanged;
        }

        private string GetWorkerFullName(string code, int plantationId)
        {
            var workerCode = CodesHelper.GetWorkerCode(code);
            IWorker worker = m_workersCache[workerCode, plantationId];
            if (worker == null)
            {
                worker = ImportedWorkers.FirstOrDefault(w => w.Code == workerCode && w.PlantationId == plantationId);
            }

            if (worker != null)
            {
                return worker.FullName;
            }

            RegisterMissingCode(workerCode, plantationId);

            return string.Empty;
        }

        private void RegisterMissingCode(string code, int plantationId)
        {
            if (!MissingCodes.Any(c => c.OldCode == CodesHelper.GetWorkerCode(code) && c.PlantationId == plantationId))
            {
                MissingCodes.Add(new WorkerCodePair()
                {
                    OldCode = CodesHelper.GetWorkerCode(code),
                    PlantationId = plantationId
                });
            }
        }

        private void RegisterDuplicatedCodes()
        {
            bool isCodeAvailable;
            foreach (var worker in ImportedWorkers)
            {
                isCodeAvailable = DatabaseHelper.IsCodeAvailable(worker.Code, worker.PlantationId);

                if (!isCodeAvailable
                    && !DuplicatedCodes.Any(c => c.OldCode == worker.Code
                    && c.PlantationId == worker.PlantationId))
                {
                    DuplicatedCodes.Add(new WorkerCodePair()
                    {
                        OldCode = worker.Code,
                        PlantationId = worker.PlantationId
                    });
                }
            }
        }

        private void DataListChanged(object sender, ListChangedEventArgs e)
        {
            if (sender.Equals(ImportedBaskets)) AggregateBaskets();
            if (sender.Equals(ImportedWorkTime)) CountHours();

            var handler = OnDataChanged;
            if (handler != null)
            {
                handler(sender, new EventArgs());
            }
        }

        public bool SaveToDB()
        {
            bool isOk = true;
            if (isOk && ImportedWorkers != null && ImportedWorkers.Any())
                isOk &= DatabaseHelper.InsertImportedWorkers(ImportedWorkers);
            if (isOk && ImportedWorkTime !=null && ImportedWorkTime.Any(wt => wt.IsImported))
                isOk &= DatabaseHelper.InsertImportedWorkTime(ImportedWorkTime.Where(wt => wt.IsImported 
                                                                                    && wt.WorkingTimeSpan.TotalMinutes > 0));
            if (isOk && AggregatedBaskets != null && AggregatedBaskets.Any())
                isOk &= DatabaseHelper.InsertImportedBaskets(AggregatedBaskets);

            if (isOk)
            {
                ArchiveFiles();
                ExportWorkersFiles();
                FileSyncHelper.PushDictionaryFiles();
            }

            return isOk;
        }

        public void UpdateMissingCodes(IEnumerable<WorkerCodePair> codesToUpdate)
        {
            if (codesToUpdate.Any())
            {
                foreach (var codeToUpdate in codesToUpdate)
                {
                    var basketsToUpdate = ImportedBaskets.Where(b => b.Code == CodesHelper.GetBasketCode(codeToUpdate.OldCode));
                    foreach (var basket in basketsToUpdate)
                    {
                        basket.Code = CodesHelper.GetBasketCode(codeToUpdate.NewCode);
                    }
                }

                AggregateBaskets();
            }
        }

        public void UpdateDuplicatedCodes(IEnumerable<WorkerCodePair> codesToUpdate)
        {
            if (codesToUpdate.Any())
            {
                foreach (var codeToUpdate in codesToUpdate)
                {
                    var workersToUpdate = ImportedWorkers.Where(w => w.Code == CodesHelper.GetWorkerCode(codeToUpdate.OldCode));
                    foreach (var worker in workersToUpdate)
                    {
                        worker.Code = CodesHelper.GetWorkerCode(codeToUpdate.NewCode);
                    }

                    var workTimeToUpdate = ImportedWorkTime.Where(t => t.Code == CodesHelper.GetWorkerCode(codeToUpdate.OldCode));
                    foreach (var workTime in workTimeToUpdate)
                    {
                        workTime.Code = CodesHelper.GetWorkerCode(codeToUpdate.NewCode);
                    }

                    var basketsToUpdate = ImportedBaskets.Where(b => b.Code == CodesHelper.GetBasketCode(codeToUpdate.OldCode));
                    foreach (var basket in basketsToUpdate)
                    {
                        basket.Code = CodesHelper.GetBasketCode(codeToUpdate.NewCode);
                    }
                }

                AggregateBaskets();
            }
        }

        private void ExportWorkersFiles()
        {
            string file_1 = Path.Combine(FileSyncHelper.LOCAL_DATA_DIRECTORY_PATH, FileSyncHelper.WORKERS_1_FILE_NAME);
            string file_2 = Path.Combine(FileSyncHelper.LOCAL_DATA_DIRECTORY_PATH, FileSyncHelper.WORKERS_2_FILE_NAME);

            var workerCodesCache = new WorkerCodesCache();

            using (var sw = new StreamWriter(file_1))
            {
                foreach (var code in workerCodesCache.Where(c => c.PlantationId == 1))
                {
                    sw.WriteLine(code.GetExportString());
                }
            }

            using (var sw = new StreamWriter(file_2))
            {
                foreach (var code in workerCodesCache.Where(c => c.PlantationId == 2))
                {
                    sw.WriteLine(code.GetExportString());
                }
            }
        }

        private void CountHours()
        {
            var groups = ImportedWorkTime
                .GroupBy(wt => new { Code = wt.Code, PlantationId = wt.PlantationId, DateString = wt.DateString });

            foreach(var grp in groups)
            {
                var minutes = grp.Where(wt=>wt.IsImported)
                    .Sum(g => (int)g.WorkingTimeSpan.TotalMinutes);
                foreach(var wt in grp)
                {
                    wt.TotalMinutes = minutes;
                }
            }
        }

        public void ParseFiles()
        {
            var files = Directory.GetFiles(FileSyncHelper.LOCAL_DATA_DIRECTORY_PATH);
            if (files.Any())
            {
                foreach (var file in files)
                {
                    var fileName = Path.GetFileNameWithoutExtension(file);
                    var fileType = FileSyncHelper.GetDataFileType(fileName);
                    if (fileType == DataFileType.Workers)
                    {
                        ParseWorkersFile(file);
                    }

                    m_fileList.Add(file);
                }

                // Two separate loops to ensure all worker files are parsed first.
                foreach (var file in files)
                {
                    var fileName = Path.GetFileNameWithoutExtension(file);
                    var fileType = FileSyncHelper.GetDataFileType(fileName);
                    if (fileType == DataFileType.Baskets)
                    {
                        ParseBasketFile(file);
                    }

                    m_fileList.Add(file);
                }
            }

            RegisterDuplicatedCodes();
        }

        public void ArchiveFiles()
        {
            EnsureArchiveDirectoryExists();
            if (m_fileList.Any())
            {
                var destinationPath = Path.Combine(LOCAL_ARCHIVE_DIRECTORY_PATH, DateTime.Now.ToString("yyyyMMddHHmmssfff"));

                foreach (var file in m_fileList)
                {
                    if (File.Exists(file))
                    {
                        var newFileName = file.Replace(FileSyncHelper.LOCAL_DATA_DIRECTORY_PATH, destinationPath);
                        File.SetAttributes(file, FileAttributes.Normal);
                        File.Move(file, newFileName);
                    }
                }
            }
        }

        private void EnsureArchiveDirectoryExists()
        {
            var dir = Path.GetDirectoryName(LOCAL_ARCHIVE_DIRECTORY_PATH);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        private void ParseWorkersFile(string file)
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            var plantationId = ExtractPlantationId(fileName);
            using (var sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var splitLine = line.Split(',');
                    if (bool.Parse(splitLine[0]) == true)
                    {
                        var worker = new ImportedWorker()
                        {
                            FirstName = splitLine[1],
                            LastName = splitLine[2],
                            Code = splitLine[3],
                            PlantationId = plantationId
                        };
                        ImportedWorkers.Add(worker);
                    }

                    ParseWorkTime(
                        plantationId, 
                        splitLine[3],
                        string.Format("{0} {1}", splitLine[1], splitLine[2]), 
                        splitLine[5]);
                }

                sr.Close();
            }
        }

        private void ParseWorkTime(int plantationId, string workerCode, string workerFullName, string workTimeString)
        {
            if (!string.IsNullOrEmpty(workTimeString))
            {
                var workTimeEntries = workTimeString.Split('|');
                foreach (var entry in workTimeEntries)
                {
                    var dates = entry.Split(':');
                    var workTime = new ImportedWorkTime()
                    {
                        PlantationId = plantationId,
                        WorkerFullName = workerFullName,
                        Code = workerCode,
                        From = ParseDateTime(dates[0]),
                        To = string.IsNullOrEmpty(dates[1]) ? (DateTime?)null : ParseDateTime(dates[1])
                    };
                    ImportedWorkTime.Add(workTime);
                }
            }
        }

        private void ParseBasketFile(string file)
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            using (var sr = new StreamReader(file))
            {
                var plantationId = ExtractPlantationId(fileName);
                var dateOfFile = ExtractDate(fileName);
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var splitLine = line.Split(',');

                    var basket = new ImportedBasket()
                    {
                        PlantationId = int.Parse(splitLine[0]),
                        Date = dateOfFile,
                        Code = splitLine[1],
                        FillingLevel = decimal.Parse(splitLine[2], NumberStyles.Any, CultureInfo.InvariantCulture)
                    };
                    ImportedBaskets.Add(basket);
                }

                sr.Close();
            }

            //AggregateBaskets();
        }

        private void AggregateBaskets()
        {
            m_aggregatedBaskets = new BindingList<BasketAggregate>(ImportedBaskets
                .Where(b => b.IsImported)
                .GroupBy(b => new
                {
                    b.PlantationId,
                    b.Code,
                    b.Date
                })
                .Select(ba => new BasketAggregate
                {
                    BasketId = -1,
                    PlantationId = ba.Key.PlantationId,
                    Code = ba.Key.Code,
                    CollectionDate = ba.Key.Date,
                    PlantationName = new PlantationsCache()[ba.Key.PlantationId].Name,
                    WorkerFullName = GetWorkerFullName(ba.Key.Code, ba.Key.PlantationId),
                    Quantity = ba.Sum(g => g.FillingLevel)

                }).ToList());
        }

        private static DateTime ExtractDate(string fileName)
        {
            DateTime date;
            DateTime.TryParseExact(fileName.Substring(10, 8), "ddMMyyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

            return date;
        }

        private static DateTime ParseDateTime(string dateTimeString)
        {
            DateTime date;
            DateTime.TryParseExact(dateTimeString, "ddMMyyyyHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

            return date;
        }

        private static int ExtractPlantationId(string fileName)
        {
            int id;
            int.TryParse(fileName.Substring(8, 1), out id);

            return id;
        }
    }
}
