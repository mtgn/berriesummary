﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BerrieSummary.Payments
{
    public partial class ChangeBaseRateForm : Form
    {
        private int m_plantationId;

        public decimal? BaseRate
        {
            get
            {
                decimal result;
                if(decimal.TryParse(BaseRateTextBox.Text, out result) && result > 0)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }
        private ChangeBaseRateForm()
        {
            InitializeComponent();
        }

        public ChangeBaseRateForm(int plantationId)
            :this()
        {
            m_plantationId = plantationId;
            BaseRateTextBox.Text = new PlantationsCache()[m_plantationId].Base_Rate.ToString("0.00");
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if(BaseRate.HasValue)
            {
                var plantation = new PlantationsCache()[m_plantationId];
                plantation.Base_Rate = BaseRate.Value;
                DatabaseHelper.UpdateBaseRate(plantation);
                Close();
            }
            else
            {
                MessageBox.Show("Stawka godzinowa musi być liczbą większą od zera.");
            }
        }
    }
}
