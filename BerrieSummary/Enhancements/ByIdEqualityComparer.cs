﻿using System.Collections.Generic;

namespace BerrieSummary
{
    public class ByIdEqualityComparer : IEqualityComparer<Worker>
    {
        public bool Equals(Worker o1, Worker o2)
        {
            return o1.WorkerId.Equals(o2.WorkerId);
        }

        public int GetHashCode(Worker o)
        {
            return o.GetHashCode();
        }
    }
}
