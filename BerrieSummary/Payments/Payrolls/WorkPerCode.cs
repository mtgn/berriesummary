﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BerrieSummary.Payments
{
    public class WorkPerCode : WorkerCode
    {
        public List<BasketAggregate> Baskets { get; set; }
    }
}
