﻿namespace BerrieSummary
{
    partial class SyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SyncForm));
            this.NextStepButton = new System.Windows.Forms.Button();
            this.wizardControl1 = new BerrieSummary.WizardControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.WorkTimeGrid = new System.Windows.Forms.DataGridView();
            this.wtCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wtWorkerFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wtDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wtIsImported = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.wtFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wtTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wtHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NewWorkersGrid = new System.Windows.Forms.DataGridView();
            this.iwPlantationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkerCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.AggregatedBasketsGrid = new System.Windows.Forms.DataGridView();
            this.abPlantationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.abWorkerFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportedBasketsGrid = new System.Windows.Forms.DataGridView();
            this.IsImported = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PlantationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FillingLevel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wizardControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkTimeGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewWorkersGrid)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AggregatedBasketsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportedBasketsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // NextStepButton
            // 
            this.NextStepButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.NextStepButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextStepButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.NextStepButton.Location = new System.Drawing.Point(355, 511);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(275, 43);
            this.NextStepButton.TabIndex = 3;
            this.NextStepButton.Text = "Dalej";
            this.NextStepButton.UseVisualStyleBackColor = false;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // wizardControl1
            // 
            this.wizardControl1.Controls.Add(this.tabPage1);
            this.wizardControl1.Controls.Add(this.tabPage2);
            this.wizardControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.wizardControl1.Location = new System.Drawing.Point(0, 0);
            this.wizardControl1.Name = "wizardControl1";
            this.wizardControl1.SelectedIndex = 0;
            this.wizardControl1.Size = new System.Drawing.Size(1024, 505);
            this.wizardControl1.TabIndex = 0;
            this.wizardControl1.SelectedIndexChanged += new System.EventHandler(this.wizardControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1016, 479);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.WorkTimeGrid, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.NewWorkersGrid, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1010, 473);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // WorkTimeGrid
            // 
            this.WorkTimeGrid.AllowUserToAddRows = false;
            this.WorkTimeGrid.AllowUserToDeleteRows = false;
            this.WorkTimeGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WorkTimeGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wtCode,
            this.wtWorkerFullName,
            this.wtDate,
            this.wtIsImported,
            this.wtFrom,
            this.wtTo,
            this.wtHours});
            this.WorkTimeGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkTimeGrid.Location = new System.Drawing.Point(508, 75);
            this.WorkTimeGrid.MultiSelect = false;
            this.WorkTimeGrid.Name = "WorkTimeGrid";
            this.WorkTimeGrid.RowHeadersVisible = false;
            this.WorkTimeGrid.Size = new System.Drawing.Size(499, 419);
            this.WorkTimeGrid.TabIndex = 2;
            this.WorkTimeGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.WorkTimeGrid_CellFormatting);
            this.WorkTimeGrid.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.WorkTimeGrid_CellMouseUp);
            this.WorkTimeGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.WorkTimeGrid_CellPainting);
            this.WorkTimeGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.WorkTimeGrid_CellValidating);
            this.WorkTimeGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.WorkTimeGrid_CellValueChanged);
            // 
            // wtCode
            // 
            this.wtCode.DataPropertyName = "Code";
            this.wtCode.HeaderText = "Kod";
            this.wtCode.Name = "wtCode";
            this.wtCode.ReadOnly = true;
            this.wtCode.Width = 50;
            // 
            // wtWorkerFullName
            // 
            this.wtWorkerFullName.DataPropertyName = "WorkerFullName";
            this.wtWorkerFullName.HeaderText = "Pracownik";
            this.wtWorkerFullName.Name = "wtWorkerFullName";
            this.wtWorkerFullName.ReadOnly = true;
            // 
            // wtDate
            // 
            this.wtDate.DataPropertyName = "DateString";
            this.wtDate.HeaderText = "Data";
            this.wtDate.Name = "wtDate";
            this.wtDate.ReadOnly = true;
            this.wtDate.Width = 80;
            // 
            // wtIsImported
            // 
            this.wtIsImported.DataPropertyName = "IsImported";
            this.wtIsImported.HeaderText = "Importuj";
            this.wtIsImported.Name = "wtIsImported";
            this.wtIsImported.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.wtIsImported.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.wtIsImported.Width = 50;
            // 
            // wtFrom
            // 
            this.wtFrom.DataPropertyName = "TimeFrom";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Orange;
            this.wtFrom.DefaultCellStyle = dataGridViewCellStyle1;
            this.wtFrom.HeaderText = "Godzina od";
            this.wtFrom.Name = "wtFrom";
            this.wtFrom.Width = 70;
            // 
            // wtTo
            // 
            this.wtTo.DataPropertyName = "TimeTo";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Orange;
            this.wtTo.DefaultCellStyle = dataGridViewCellStyle2;
            this.wtTo.HeaderText = "Godzina do";
            this.wtTo.Name = "wtTo";
            this.wtTo.Width = 70;
            // 
            // wtHours
            // 
            this.wtHours.DataPropertyName = "TotalTimeString";
            this.wtHours.HeaderText = "Razem";
            this.wtHours.Name = "wtHours";
            this.wtHours.ReadOnly = true;
            this.wtHours.Width = 50;
            // 
            // NewWorkersGrid
            // 
            this.NewWorkersGrid.AllowUserToAddRows = false;
            this.NewWorkersGrid.AllowUserToDeleteRows = false;
            this.NewWorkersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NewWorkersGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iwPlantationName,
            this.WorkerCode,
            this.FirstName,
            this.LastName});
            this.NewWorkersGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewWorkersGrid.Location = new System.Drawing.Point(3, 75);
            this.NewWorkersGrid.MultiSelect = false;
            this.NewWorkersGrid.Name = "NewWorkersGrid";
            this.NewWorkersGrid.RowHeadersVisible = false;
            this.NewWorkersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.NewWorkersGrid.Size = new System.Drawing.Size(499, 419);
            this.NewWorkersGrid.TabIndex = 4;
            // 
            // iwPlantationName
            // 
            this.iwPlantationName.DataPropertyName = "PlantationName";
            this.iwPlantationName.HeaderText = "Plantacja";
            this.iwPlantationName.Name = "iwPlantationName";
            this.iwPlantationName.ReadOnly = true;
            // 
            // WorkerCode
            // 
            this.WorkerCode.DataPropertyName = "Code";
            this.WorkerCode.HeaderText = "Kod";
            this.WorkerCode.Name = "WorkerCode";
            this.WorkerCode.ReadOnly = true;
            this.WorkerCode.Width = 70;
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Orange;
            this.FirstName.DefaultCellStyle = dataGridViewCellStyle3;
            this.FirstName.HeaderText = "Imię";
            this.FirstName.Name = "FirstName";
            // 
            // LastName
            // 
            this.LastName.DataPropertyName = "LastName";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Orange;
            this.LastName.DefaultCellStyle = dataGridViewCellStyle4;
            this.LastName.HeaderText = "Nazwisko";
            this.LastName.Name = "LastName";
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Image = global::BerrieSummary.Properties.Resources.workers;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(348, 72);
            this.label1.TabIndex = 5;
            this.label1.Text = "Import nowych pracowników";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Image = global::BerrieSummary.Properties.Resources.time;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(508, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 72);
            this.label2.TabIndex = 6;
            this.label2.Text = "Import czasu pracy";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(976, 479);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.AggregatedBasketsGrid, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.ImportedBasketsGrid, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(970, 473);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Image = global::BerrieSummary.Properties.Resources.basket;
            this.label4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(240, 72);
            this.label4.TabIndex = 7;
            this.label4.Text = "Import koszyków";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AggregatedBasketsGrid
            // 
            this.AggregatedBasketsGrid.AllowUserToAddRows = false;
            this.AggregatedBasketsGrid.AllowUserToDeleteRows = false;
            this.AggregatedBasketsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AggregatedBasketsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.abPlantationName,
            this.abDate,
            this.abCode,
            this.abWorkerFullName,
            this.Quantity});
            this.AggregatedBasketsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AggregatedBasketsGrid.Location = new System.Drawing.Point(488, 75);
            this.AggregatedBasketsGrid.Name = "AggregatedBasketsGrid";
            this.AggregatedBasketsGrid.ReadOnly = true;
            this.AggregatedBasketsGrid.RowHeadersVisible = false;
            this.AggregatedBasketsGrid.Size = new System.Drawing.Size(479, 395);
            this.AggregatedBasketsGrid.TabIndex = 2;
            // 
            // abPlantationName
            // 
            this.abPlantationName.DataPropertyName = "PlantationName";
            this.abPlantationName.HeaderText = "Plantacja";
            this.abPlantationName.Name = "abPlantationName";
            this.abPlantationName.ReadOnly = true;
            // 
            // abDate
            // 
            this.abDate.DataPropertyName = "CollectionDate";
            dataGridViewCellStyle5.Format = "dd-MM-yyyy";
            dataGridViewCellStyle5.NullValue = null;
            this.abDate.DefaultCellStyle = dataGridViewCellStyle5;
            this.abDate.HeaderText = "Data";
            this.abDate.Name = "abDate";
            this.abDate.ReadOnly = true;
            // 
            // abCode
            // 
            this.abCode.DataPropertyName = "Code";
            this.abCode.HeaderText = "Kod";
            this.abCode.Name = "abCode";
            this.abCode.ReadOnly = true;
            // 
            // abWorkerFullName
            // 
            this.abWorkerFullName.DataPropertyName = "WorkerFullName";
            this.abWorkerFullName.HeaderText = "Pracownik";
            this.abWorkerFullName.Name = "abWorkerFullName";
            this.abWorkerFullName.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            dataGridViewCellStyle6.Format = "N1";
            dataGridViewCellStyle6.NullValue = null;
            this.Quantity.DefaultCellStyle = dataGridViewCellStyle6;
            this.Quantity.HeaderText = "Ilość";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.Width = 50;
            // 
            // ImportedBasketsGrid
            // 
            this.ImportedBasketsGrid.AllowUserToAddRows = false;
            this.ImportedBasketsGrid.AllowUserToDeleteRows = false;
            this.ImportedBasketsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ImportedBasketsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IsImported,
            this.PlantationName,
            this.Date,
            this.Code,
            this.FillingLevel});
            this.ImportedBasketsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImportedBasketsGrid.Location = new System.Drawing.Point(3, 75);
            this.ImportedBasketsGrid.MultiSelect = false;
            this.ImportedBasketsGrid.Name = "ImportedBasketsGrid";
            this.ImportedBasketsGrid.RowHeadersVisible = false;
            this.ImportedBasketsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.ImportedBasketsGrid.Size = new System.Drawing.Size(479, 395);
            this.ImportedBasketsGrid.TabIndex = 1;
            this.ImportedBasketsGrid.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ImportedBasketsGrid_CellMouseUp);
            this.ImportedBasketsGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ImportedBasketsGrid_CellValidating);
            this.ImportedBasketsGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.ImportedBasketsGrid_CellValueChanged);
            this.ImportedBasketsGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.ImportedBasketsGrid_DataError);
            // 
            // IsImported
            // 
            this.IsImported.DataPropertyName = "IsImported";
            this.IsImported.HeaderText = "Importuj";
            this.IsImported.Name = "IsImported";
            this.IsImported.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsImported.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.IsImported.Width = 50;
            // 
            // PlantationName
            // 
            this.PlantationName.DataPropertyName = "PlantationName";
            this.PlantationName.HeaderText = "Plantacja";
            this.PlantationName.Name = "PlantationName";
            this.PlantationName.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "DateString";
            this.Date.HeaderText = "Data";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // Code
            // 
            this.Code.DataPropertyName = "Code";
            this.Code.HeaderText = "Kod";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            // 
            // FillingLevel
            // 
            this.FillingLevel.DataPropertyName = "FillingLevelString";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle7.Format = "N1";
            dataGridViewCellStyle7.NullValue = "1.0";
            this.FillingLevel.DefaultCellStyle = dataGridViewCellStyle7;
            this.FillingLevel.HeaderText = "%";
            this.FillingLevel.Name = "FillingLevel";
            this.FillingLevel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FillingLevel.Width = 50;
            // 
            // SyncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 561);
            this.Controls.Add(this.NextStepButton);
            this.Controls.Add(this.wizardControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SyncForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Synchronizacja danych";
            this.wizardControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WorkTimeGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewWorkersGrid)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AggregatedBasketsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImportedBasketsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private WizardControl wizardControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.DataGridView WorkTimeGrid;
        private System.Windows.Forms.DataGridView ImportedBasketsGrid;
        private System.Windows.Forms.DataGridView AggregatedBasketsGrid;
        private System.Windows.Forms.DataGridView NewWorkersGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsImported;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlantationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn FillingLevel;
        private System.Windows.Forms.DataGridViewTextBoxColumn abPlantationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn abDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn abCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn abWorkerFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn iwPlantationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkerCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn wtCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn wtWorkerFullName;
        private System.Windows.Forms.DataGridViewTextBoxColumn wtDate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn wtIsImported;
        private System.Windows.Forms.DataGridViewTextBoxColumn wtFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn wtTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn wtHours;
    }
}