﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BerrieSummary
{
    public partial class BackupForm : Form
    {
        private int m_backupsCompleted = 0;
        public BackupForm()
        {
            InitializeComponent();

            new Thread(() =>
            {
                //Thread.CurrentThread.IsBackground = true;
                DatabaseHelper.OnBackupProgress += DatabaseHelper_OnBackupProgress;
                try
                {
                    DatabaseHelper.CreateBackup();
                }
                catch(Exception)
                {
                    MessageBox.Show("Nie udało się wykonać kopii zapasowej.\nProgram może nie mieć uprawnień do zapisu w wybranej lokalizacji.");
                    Invoke(new MethodInvoker(() => Close()));
                }
            }).Start();
        }

        private void DatabaseHelper_OnBackupProgress(object sender, BackupProgressEventArgs e)
        {
            if (e.Percent == 100)
            {
                m_backupsCompleted++;
            }

            progressBar1.Invoke(new MethodInvoker(() =>
            {
                progressBar1.Value = e.Percent;

            }));

            if (m_backupsCompleted == 2)
            {
                DatabaseHelper.OnBackupProgress -= DatabaseHelper_OnBackupProgress;
                Invoke(new MethodInvoker(() => Close()));
            }
        }
    }
}
