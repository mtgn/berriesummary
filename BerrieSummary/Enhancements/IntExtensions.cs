﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class IntExtensions
    {
        public static string FromMinutesToTimeString(this int minutes)
        {
            return string.Format("{0} h {1}",
                    (int)(minutes / 60),
                    (minutes % 60) != 0
                        ? (minutes % 60).ToString("00 m")
                        : string.Empty
                    ).Trim();
        }
    }
}
