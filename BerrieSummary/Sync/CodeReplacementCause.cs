﻿namespace BerrieSummary.Sync
{
    public enum CodeReplacementCause
    {
        Missing,
        Duplicated
    }
}