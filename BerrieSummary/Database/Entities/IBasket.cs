﻿using System;

namespace BerrieSummary
{
    public interface IBasket
    {
        int BasketId { get; set; }
        int CodeId { get; set; }
        int PlantationId { get; set; }
        decimal Quantity { get; set; }
        DateTime CollectionDate { get; set; }
    }
}