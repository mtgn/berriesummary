﻿using BerrieSummary.Harvests;
using BerrieSummary.Workers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BerrieSummary
{
    public class WorkerCode
    {
        public int CodeId { get; set; }
        public int WorkerId { get; set; }
        public int PlantationId { get; set; }
        public string Code { get; set; }

        public string GetExportString()
        {
            var worker = DatabaseHelper.GetWorkerById(WorkerId);
            var historyHelper = new WorkerHistoryHelper(PlantationId, CodeId);
            var historyDict = historyHelper.GetRecentHistory(5);
            var historyString = CreateRecentHistoryString(historyDict);
            return string.Format("False,{0},{1},{2},{3},", worker.FirstName, worker.LastName, Code, historyString);
        }

        public string CreateRecentHistoryString(Dictionary<DateTime, string> historyDictionary)
        {
            var sb = new StringBuilder();
            foreach(var eff in historyDictionary)
            {
                if(sb.Length > 0)
                {
                    sb.Append('|');
                }

                sb.AppendFormat("{0:ddMMyyyy}:{1}", eff.Key, eff.Value);
            }

            return sb.ToString();
        }
    }
}