﻿namespace BerrieSummary.Harvests
{
    partial class HarvestsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HarvestsForm));
            this.HarvestDaysSummaryGrid = new System.Windows.Forms.DataGridView();
            this.hDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hWorkersCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hBasketsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hWorkTimeCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hAverageEfficiency = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hDayCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hBasketCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hBasketRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hDailyPayment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.HarvestDaysSummaryGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // HarvestDaysSummaryGrid
            // 
            this.HarvestDaysSummaryGrid.AllowUserToAddRows = false;
            this.HarvestDaysSummaryGrid.AllowUserToDeleteRows = false;
            this.HarvestDaysSummaryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HarvestDaysSummaryGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.hDate,
            this.hWorkersCount,
            this.hBasketsCount,
            this.hWorkTimeCount,
            this.hAverageEfficiency,
            this.hDayCost,
            this.hBasketCost,
            this.hBasketRate,
            this.hDailyPayment});
            this.HarvestDaysSummaryGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HarvestDaysSummaryGrid.Location = new System.Drawing.Point(0, 0);
            this.HarvestDaysSummaryGrid.MultiSelect = false;
            this.HarvestDaysSummaryGrid.Name = "HarvestDaysSummaryGrid";
            this.HarvestDaysSummaryGrid.RowHeadersVisible = false;
            this.HarvestDaysSummaryGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.HarvestDaysSummaryGrid.Size = new System.Drawing.Size(788, 454);
            this.HarvestDaysSummaryGrid.TabIndex = 0;
            this.HarvestDaysSummaryGrid.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.HarvestDaysSummaryGrid_CellBeginEdit);
            this.HarvestDaysSummaryGrid.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.HarvestDaysSummaryGrid_CellValidating);
            this.HarvestDaysSummaryGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HarvestDaysSummaryGrid_CellValueChanged);
            this.HarvestDaysSummaryGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.HarvestDaysSummaryGrid_ColumnHeaderMouseClick);
            this.HarvestDaysSummaryGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.HarvestDaysSummaryGrid_DataError);
            // 
            // hDate
            // 
            this.hDate.DataPropertyName = "DateOfHarvestString";
            this.hDate.HeaderText = "Data";
            this.hDate.MaxInputLength = 10;
            this.hDate.Name = "hDate";
            this.hDate.ReadOnly = true;
            this.hDate.Width = 70;
            // 
            // hWorkersCount
            // 
            this.hWorkersCount.DataPropertyName = "WorkersCount";
            this.hWorkersCount.HeaderText = "Suma pracowników";
            this.hWorkersCount.Name = "hWorkersCount";
            this.hWorkersCount.ReadOnly = true;
            this.hWorkersCount.Width = 95;
            // 
            // hBasketsCount
            // 
            this.hBasketsCount.DataPropertyName = "BasketsCount";
            dataGridViewCellStyle1.Format = "0.00";
            this.hBasketsCount.DefaultCellStyle = dataGridViewCellStyle1;
            this.hBasketsCount.HeaderText = "Suma koszyków (A)";
            this.hBasketsCount.Name = "hBasketsCount";
            this.hBasketsCount.ReadOnly = true;
            this.hBasketsCount.Width = 80;
            // 
            // hWorkTimeCount
            // 
            this.hWorkTimeCount.DataPropertyName = "WorkTimeCountString";
            this.hWorkTimeCount.HeaderText = "Suma czasu pracy (B)";
            this.hWorkTimeCount.Name = "hWorkTimeCount";
            this.hWorkTimeCount.ReadOnly = true;
            this.hWorkTimeCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.hWorkTimeCount.Width = 80;
            // 
            // hAverageEfficiency
            // 
            this.hAverageEfficiency.DataPropertyName = "AverageTempo";
            dataGridViewCellStyle2.Format = "0.00";
            this.hAverageEfficiency.DefaultCellStyle = dataGridViewCellStyle2;
            this.hAverageEfficiency.HeaderText = "Średnie tempo zbierania (A/B)";
            this.hAverageEfficiency.Name = "hAverageEfficiency";
            this.hAverageEfficiency.ReadOnly = true;
            this.hAverageEfficiency.Width = 80;
            // 
            // hDayCost
            // 
            this.hDayCost.DataPropertyName = "DayCost";
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.Format = "0 zł";
            this.hDayCost.DefaultCellStyle = dataGridViewCellStyle3;
            this.hDayCost.HeaderText = "Koszt całego zbioru (C)";
            this.hDayCost.Name = "hDayCost";
            this.hDayCost.ReadOnly = true;
            this.hDayCost.Width = 80;
            // 
            // hBasketCost
            // 
            this.hBasketCost.DataPropertyName = "BasketCost";
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.Format = "0.00 zł";
            this.hBasketCost.DefaultCellStyle = dataGridViewCellStyle4;
            this.hBasketCost.HeaderText = "Koszt koszyka (C/A)";
            this.hBasketCost.Name = "hBasketCost";
            this.hBasketCost.ReadOnly = true;
            this.hBasketCost.Width = 80;
            // 
            // hBasketRate
            // 
            this.hBasketRate.DataPropertyName = "BasketRate";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Orange;
            dataGridViewCellStyle5.Format = "0.00 zł";
            dataGridViewCellStyle5.NullValue = null;
            this.hBasketRate.DefaultCellStyle = dataGridViewCellStyle5;
            this.hBasketRate.HeaderText = "Stawka za koszyk (D)";
            this.hBasketRate.MaxInputLength = 11;
            this.hBasketRate.Name = "hBasketRate";
            // 
            // hDailyPayment
            // 
            this.hDailyPayment.DataPropertyName = "DailyPayment";
            dataGridViewCellStyle6.Format = "0 zł";
            dataGridViewCellStyle6.NullValue = null;
            this.hDailyPayment.DefaultCellStyle = dataGridViewCellStyle6;
            this.hDailyPayment.HeaderText = "Suma wynagrodzeń (A*D)";
            this.hDailyPayment.Name = "hDailyPayment";
            this.hDailyPayment.ReadOnly = true;
            // 
            // HarvestsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 454);
            this.Controls.Add(this.HarvestDaysSummaryGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "HarvestsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Zbiory";
            ((System.ComponentModel.ISupportInitialize)(this.HarvestDaysSummaryGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView HarvestDaysSummaryGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn hWorkersCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn hBasketsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn hWorkTimeCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn hAverageEfficiency;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDayCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn hBasketCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn hBasketRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDailyPayment;
    }
}