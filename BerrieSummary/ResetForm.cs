﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary
{
    public partial class ResetForm : Form
    {
        public ResetForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "tak")
            {
                using (var connection = new SqlConnection("server=(local)\\SQLEXPRESS;Trusted_Connection=yes"))
                {
                    connection.Open();
                    using (var command = new SqlCommand($"drop database {Program.DATABASE_NAME}", connection))
                    {
                        if(command.ExecuteScalar() != DBNull.Value)
                        {
                            if(MessageBox.Show("Baza danych została usunięta. Uruchom ponownie aplikację.") == DialogResult.OK)
                            {
                                Application.Restart();
                            }
                        }
                    }
                }
            }
        }
    }
}
