﻿namespace BerrieSummary.Workers
{
    partial class WorkersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorkersForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.WorkersGrid = new System.Windows.Forms.DataGridView();
            this.wWorkerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.WorkerHistoryButton = new System.Windows.Forms.Button();
            this.PickWorkerButton = new System.Windows.Forms.Button();
            this.DeleteWorkerButton = new System.Windows.Forms.Button();
            this.PreviewWorkerButton = new System.Windows.Forms.Button();
            this.EditWorkerButton = new System.Windows.Forms.Button();
            this.AddWorkerButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkersGrid)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.Controls.Add(this.WorkersGrid, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(574, 527);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // WorkersGrid
            // 
            this.WorkersGrid.AllowUserToAddRows = false;
            this.WorkersGrid.AllowUserToDeleteRows = false;
            this.WorkersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WorkersGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wWorkerId,
            this.wCode,
            this.wFirstName,
            this.wLastName});
            this.WorkersGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkersGrid.Location = new System.Drawing.Point(3, 3);
            this.WorkersGrid.MultiSelect = false;
            this.WorkersGrid.Name = "WorkersGrid";
            this.WorkersGrid.ReadOnly = true;
            this.WorkersGrid.RowHeadersVisible = false;
            this.WorkersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WorkersGrid.Size = new System.Drawing.Size(326, 521);
            this.WorkersGrid.TabIndex = 1;
            // 
            // wWorkerId
            // 
            this.wWorkerId.DataPropertyName = "WorkerId";
            this.wWorkerId.HeaderText = "ID";
            this.wWorkerId.Name = "wWorkerId";
            this.wWorkerId.ReadOnly = true;
            this.wWorkerId.Visible = false;
            // 
            // wCode
            // 
            this.wCode.DataPropertyName = "Code";
            this.wCode.HeaderText = "Kod";
            this.wCode.Name = "wCode";
            this.wCode.ReadOnly = true;
            // 
            // wFirstName
            // 
            this.wFirstName.DataPropertyName = "FirstName";
            this.wFirstName.HeaderText = "Imię";
            this.wFirstName.Name = "wFirstName";
            this.wFirstName.ReadOnly = true;
            // 
            // wLastName
            // 
            this.wLastName.DataPropertyName = "LastName";
            this.wLastName.HeaderText = "Nazwisko";
            this.wLastName.Name = "wLastName";
            this.wLastName.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.WorkerHistoryButton);
            this.panel1.Controls.Add(this.PickWorkerButton);
            this.panel1.Controls.Add(this.DeleteWorkerButton);
            this.panel1.Controls.Add(this.PreviewWorkerButton);
            this.panel1.Controls.Add(this.EditWorkerButton);
            this.panel1.Controls.Add(this.AddWorkerButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(335, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 521);
            this.panel1.TabIndex = 2;
            // 
            // WorkerHistoryButton
            // 
            this.WorkerHistoryButton.BackColor = System.Drawing.Color.Orange;
            this.WorkerHistoryButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WorkerHistoryButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.WorkerHistoryButton.ForeColor = System.Drawing.Color.Black;
            this.WorkerHistoryButton.Location = new System.Drawing.Point(33, 139);
            this.WorkerHistoryButton.Name = "WorkerHistoryButton";
            this.WorkerHistoryButton.Size = new System.Drawing.Size(164, 74);
            this.WorkerHistoryButton.TabIndex = 15;
            this.WorkerHistoryButton.Text = "Historia pracy";
            this.WorkerHistoryButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.WorkerHistoryButton.UseVisualStyleBackColor = false;
            this.WorkerHistoryButton.Click += new System.EventHandler(this.WorkerHistoryButton_Click);
            // 
            // PickWorkerButton
            // 
            this.PickWorkerButton.BackColor = System.Drawing.Color.LimeGreen;
            this.PickWorkerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PickWorkerButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PickWorkerButton.ForeColor = System.Drawing.Color.Black;
            this.PickWorkerButton.Location = new System.Drawing.Point(33, 339);
            this.PickWorkerButton.Name = "PickWorkerButton";
            this.PickWorkerButton.Size = new System.Drawing.Size(164, 171);
            this.PickWorkerButton.TabIndex = 14;
            this.PickWorkerButton.Text = "Przypisz pracownika";
            this.PickWorkerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PickWorkerButton.UseVisualStyleBackColor = false;
            this.PickWorkerButton.Visible = false;
            this.PickWorkerButton.Click += new System.EventHandler(this.PickWorkerButton_Click);
            // 
            // DeleteWorkerButton
            // 
            this.DeleteWorkerButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.DeleteWorkerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteWorkerButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DeleteWorkerButton.ForeColor = System.Drawing.Color.Black;
            this.DeleteWorkerButton.Location = new System.Drawing.Point(33, 438);
            this.DeleteWorkerButton.Name = "DeleteWorkerButton";
            this.DeleteWorkerButton.Size = new System.Drawing.Size(164, 72);
            this.DeleteWorkerButton.TabIndex = 13;
            this.DeleteWorkerButton.Text = "Usuń";
            this.DeleteWorkerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.DeleteWorkerButton.UseVisualStyleBackColor = false;
            this.DeleteWorkerButton.Click += new System.EventHandler(this.DeleteWorkerButton_Click);
            // 
            // PreviewWorkerButton
            // 
            this.PreviewWorkerButton.BackColor = System.Drawing.Color.Orange;
            this.PreviewWorkerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PreviewWorkerButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PreviewWorkerButton.ForeColor = System.Drawing.Color.Black;
            this.PreviewWorkerButton.Location = new System.Drawing.Point(33, 37);
            this.PreviewWorkerButton.Name = "PreviewWorkerButton";
            this.PreviewWorkerButton.Size = new System.Drawing.Size(164, 74);
            this.PreviewWorkerButton.TabIndex = 12;
            this.PreviewWorkerButton.Text = "Karta pracownika";
            this.PreviewWorkerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.PreviewWorkerButton.UseVisualStyleBackColor = false;
            this.PreviewWorkerButton.Click += new System.EventHandler(this.PreviewWorkerButton_Click);
            // 
            // EditWorkerButton
            // 
            this.EditWorkerButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.EditWorkerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EditWorkerButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.EditWorkerButton.ForeColor = System.Drawing.Color.Black;
            this.EditWorkerButton.Location = new System.Drawing.Point(33, 339);
            this.EditWorkerButton.Name = "EditWorkerButton";
            this.EditWorkerButton.Size = new System.Drawing.Size(164, 72);
            this.EditWorkerButton.TabIndex = 11;
            this.EditWorkerButton.Text = "Edytuj";
            this.EditWorkerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.EditWorkerButton.UseVisualStyleBackColor = false;
            this.EditWorkerButton.Click += new System.EventHandler(this.EditWorkerButton_Click);
            // 
            // AddWorkerButton
            // 
            this.AddWorkerButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.AddWorkerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddWorkerButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AddWorkerButton.ForeColor = System.Drawing.Color.Black;
            this.AddWorkerButton.Location = new System.Drawing.Point(33, 239);
            this.AddWorkerButton.Name = "AddWorkerButton";
            this.AddWorkerButton.Size = new System.Drawing.Size(164, 72);
            this.AddWorkerButton.TabIndex = 10;
            this.AddWorkerButton.Text = "Dodaj";
            this.AddWorkerButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.AddWorkerButton.UseVisualStyleBackColor = false;
            this.AddWorkerButton.Click += new System.EventHandler(this.AddWorkerButton_Click);
            // 
            // WorkersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 527);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "WorkersForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pracownicy";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WorkersGrid)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridView WorkersGrid;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button DeleteWorkerButton;
        private System.Windows.Forms.Button PreviewWorkerButton;
        private System.Windows.Forms.Button EditWorkerButton;
        private System.Windows.Forms.Button AddWorkerButton;
        private System.Windows.Forms.Button PickWorkerButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn wWorkerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn wCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn wFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn wLastName;
        private System.Windows.Forms.Button WorkerHistoryButton;
    }
}