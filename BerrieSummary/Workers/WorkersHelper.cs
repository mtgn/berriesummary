﻿using System;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary.Workers
{
    public class WorkersHelper
    {
        private int m_plantationId = -1;
        private WorkersCache m_workersCache;

        public SortableBindingList<Worker> Workers { get; private set; }

        public WorkersHelper(int plantationId)
        {
            m_plantationId = plantationId;
            CreateWorkersList();
        }

        private void CreateWorkersList()
        {
            m_workersCache = new WorkersCache(m_plantationId);
            Workers = new SortableBindingList<Worker>(m_workersCache.OrderBy(w=>w.Code).ToList());
        }

        public void RefreshCache()
        {
            CreateWorkersList();
        }
    }
}