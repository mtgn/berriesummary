﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary
{
    public class WorkTimeCache : IEnumerable<WorkTime>
    {
        private BindingList<WorkTime> m_workTimeList = new BindingList<WorkTime>();

        public WorkTimeCache(int plantationId)
        {
            m_workTimeList = DatabaseHelper.GetWorkTime(plantationId);
        }

        public WorkTimeCache(int plantationId, int codeId)
        {
            m_workTimeList = DatabaseHelper.GetWorkTime(plantationId, codeId);
        }

        public WorkTime this[int id]
        {
            get
            {
                return m_workTimeList.FirstOrDefault(t => t.WorkTimeId == id);
            }
        }

        public IEnumerable<WorkTime> this[DateTime date]
        {
            get
            {
                return m_workTimeList.Where(t => t.Date == date);
            }
        }

        public IEnumerator<WorkTime> GetEnumerator()
        {
            return m_workTimeList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}