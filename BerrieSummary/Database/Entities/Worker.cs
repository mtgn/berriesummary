﻿using BerrieSummary.Sync;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace BerrieSummary
{
    public class Worker : IWorker
    {
        public int WorkerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string InsuranceInfo { get; set; }
        public string Nip { get; set; }
        public string Pesel { get; set; }
        public string RevenueOffice { get; set; }

        public bool IsActive { get; set; }
        public string Code { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName).Trim();
            }
        }

        public Worker()
        {
            IsActive = true;
        }
    }
}