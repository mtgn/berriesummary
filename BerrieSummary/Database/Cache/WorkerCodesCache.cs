﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary
{
    public class WorkerCodesCache : IEnumerable<WorkerCode>
    {
        private BindingList<WorkerCode> m_workerCodes;

        public WorkerCodesCache()
        {
            m_workerCodes = DatabaseHelper.GetWorkerCodes();
        }

        public WorkerCodesCache(int plantationId)
        {
            m_workerCodes = DatabaseHelper.GetWorkerCodes(plantationId);
        }

        public WorkerCode this[int id]
        {
            get
            {
                return m_workerCodes.FirstOrDefault(p => p.CodeId == id);
            }
        }

        public WorkerCode this[string code]
        {
            get
            {
                return m_workerCodes.FirstOrDefault(w => w.Code == CodesHelper.GetWorkerCode(code));
            }
        }

        public WorkerCode this[string code, int plantationId]
        {
            get
            {
                return m_workerCodes.FirstOrDefault(w => w.Code == CodesHelper.GetWorkerCode(code) && w.PlantationId == plantationId);
            }
        }

        public IEnumerable<WorkerCode> GetByWorkerId(int workerId)
        {
            return m_workerCodes.Where(c => c.WorkerId == workerId);
        }

        public IEnumerator<WorkerCode> GetEnumerator()
        {
            return m_workerCodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}