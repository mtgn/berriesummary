﻿namespace BerrieSummary.Sync
{
    partial class CodesReplaceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodesReplaceForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.PickWorkerButton = new System.Windows.Forms.Button();
            this.mcPlantationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mcOldCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mcNewCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mcPlantationName,
            this.mcOldCode,
            this.mcNewCode});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(347, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ConfirmButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConfirmButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ConfirmButton.Location = new System.Drawing.Point(197, 185);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(162, 59);
            this.ConfirmButton.TabIndex = 4;
            this.ConfirmButton.Text = "Zatwierdź";
            this.ConfirmButton.UseVisualStyleBackColor = false;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // PickWorkerButton
            // 
            this.PickWorkerButton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.PickWorkerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PickWorkerButton.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PickWorkerButton.Location = new System.Drawing.Point(12, 184);
            this.PickWorkerButton.Name = "PickWorkerButton";
            this.PickWorkerButton.Size = new System.Drawing.Size(162, 59);
            this.PickWorkerButton.TabIndex = 5;
            this.PickWorkerButton.Text = "Przypisz pracownika";
            this.PickWorkerButton.UseVisualStyleBackColor = false;
            this.PickWorkerButton.Click += new System.EventHandler(this.PickWorkerButton_Click);
            // 
            // mcPlantationName
            // 
            this.mcPlantationName.DataPropertyName = "PlantationName";
            this.mcPlantationName.HeaderText = "Plantacja";
            this.mcPlantationName.Name = "mcPlantationName";
            this.mcPlantationName.ReadOnly = true;
            // 
            // mcOldCode
            // 
            this.mcOldCode.DataPropertyName = "OldCode";
            this.mcOldCode.HeaderText = "Brakujący kod";
            this.mcOldCode.Name = "mcOldCode";
            this.mcOldCode.ReadOnly = true;
            this.mcOldCode.Width = 110;
            // 
            // mcNewCode
            // 
            this.mcNewCode.DataPropertyName = "NewCode";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Orange;
            this.mcNewCode.DefaultCellStyle = dataGridViewCellStyle1;
            this.mcNewCode.HeaderText = "Nowy kod";
            this.mcNewCode.Name = "mcNewCode";
            this.mcNewCode.ReadOnly = true;
            // 
            // CodesReplaceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 264);
            this.Controls.Add(this.PickWorkerButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CodesReplaceForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Brakujące kody";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.Button PickWorkerButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn mcPlantationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn mcOldCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn mcNewCode;
    }
}