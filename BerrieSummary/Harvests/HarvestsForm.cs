﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BerrieSummary.Harvests
{
    public partial class HarvestsForm : Form
    {
        private HarvestsHelper m_harvestsHelper = null;

        private HarvestsForm()
        {
            InitializeComponent();
            HarvestDaysSummaryGrid.AutoGenerateColumns = false;
        }

        public HarvestsForm(int plantationId)
            :this()
        {
            m_harvestsHelper = new HarvestsHelper(plantationId);
            HarvestDaysSummaryGrid.DataSource = m_harvestsHelper.HarvestsSummary;
        }

        private void HarvestDaysSummaryGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            
        }

        private void HarvestDaysSummaryGrid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex != hBasketRate.Index)
            {
                return;
            }

            var value = e.FormattedValue.ToString();

            var indexOfCurrencySymbol = value.IndexOf(" zł");
            if(indexOfCurrencySymbol >= 0)
            {
                value = value.Remove(indexOfCurrencySymbol);
            }

            if (!string.IsNullOrEmpty(value))
            {
                decimal result = 0;
                if (!decimal.TryParse(value, out result) || result <= 0)
                {
                    MessageBox.Show("Stawka za koszyk musi być liczbą większą od zera.");
                    e.Cancel = true;
                }
            }
        }

        private void HarvestDaysSummaryGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            var harvestDaySummary = m_harvestsHelper.HarvestsSummary[e.RowIndex];
            if (DatabaseHelper.PaymentExists(harvestDaySummary.PlantationId, harvestDaySummary.DateOfHarvest))
            {
                MessageBox.Show("Nie można edytować stawki dla dnia, który został objęty wypłatą.");
                e.Cancel = true;
                HarvestDaysSummaryGrid.CancelEdit();
            }
        }

        private void HarvestDaysSummaryGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (HarvestDaysSummaryGrid.IsCurrentCellInEditMode && e.ColumnIndex == hBasketRate.Index)
            {
                var harvestDaySummary = m_harvestsHelper.HarvestsSummary[e.RowIndex];
                m_harvestsHelper.SetBasketRate(harvestDaySummary.DateOfHarvest, harvestDaySummary.BasketRate);
                HarvestDaysSummaryGrid.InvalidateCell(HarvestDaysSummaryGrid[hDailyPayment.Index, e.RowIndex]);
            }
        }

        private void HarvestDaysSummaryGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == hWorkTimeCount.Index)
            {
                (HarvestDaysSummaryGrid.DataSource as SortableBindingList<HarvestDaySummary>).Sort("WorkTimeMinutesCount");
            }
        }
    }
}
