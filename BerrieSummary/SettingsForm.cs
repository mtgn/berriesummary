﻿using BerrieSummary.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BerrieSummary
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
            Settings.Default.Upgrade();
            RefreshBackupControls();
        }

        private void RefreshBackupControls()
        {
            textBox1.Text = folderBrowserDialog1.SelectedPath = Settings.Default.BackupDirectory;
        }

        private void BackupPickFolderButton_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                Settings.Default["BackupDirectory"] = folderBrowserDialog1.SelectedPath;
                RefreshBackupControls();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();
            Close();
        }
    }
}
