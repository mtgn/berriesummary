﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BerrieSummary
{
    public class PaymentsCache : IEnumerable<Payment>
    {
        private BindingList<Payment> m_payments = new BindingList<Payment>();

        public PaymentsCache(int plantationId)
        {
            m_payments = DatabaseHelper.GetPayments(plantationId);
        }

        public Payment this[int id]
        {
            get
            {
                return m_payments.FirstOrDefault(p => p.PaymentId == id);
            }
        }

        public IEnumerator<Payment> GetEnumerator()
        {
            return m_payments.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}